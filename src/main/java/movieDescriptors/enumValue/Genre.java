package movieDescriptors.enumValue;

import basics.enuminterface.StringEnum;

public enum Genre implements StringEnum {
  ACTION("action"),
  ROMANCE("romance"),
  HORROR("horror"),
  SPLATTER("splatter"),
  COMEDY("comedy"),
  DRAMA("drama"),
  DOCUMENTARY("documentary");

  private final String _dbValue;

  Genre(String dbValue) {
    this._dbValue = dbValue;
  }

  public String dbValue() {
    return _dbValue;
  }
}
