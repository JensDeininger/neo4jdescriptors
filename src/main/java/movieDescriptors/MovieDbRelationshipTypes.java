package movieDescriptors;

import basics.relationship.*;
import basics.repository.RelationshipTypeDescriptorRepository;

public class MovieDbRelationshipTypes
  extends RelationshipTypeDescriptorRepository {

  // <stuff that can be copy pasted from a templates and will be the same whatever Database one wants to describe>

  private static MovieDbRelationshipTypes INSTANCE;

  public static MovieDbRelationshipTypes getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new MovieDbRelationshipTypes();
    }
    return INSTANCE;
  }

  private MovieDbRelationshipTypes() {
    super();
  }

  // </stuff that can be copy pasted from a templates and will be the same whatever Database one wants to describe>

  /* All RelationshipTypeDescriptor one wishes to use must be listed here.
   * Since this class is a singleton, they will be available everywhere by calls like
   * MovieDbRelationshipTypes.getInstance().ACTED_IN
   *
   * This class also inherits the method   *
   * public Stream<RelationshipTypeDescriptor> relationshipTypeDescriptors()
   * which will return all RelationshipTypeDescriptor defined here.
   */

  public RelationshipTypeDescriptor ACTED_IN = new RelationshipTypeDescriptor(
    "ACTED_IN"
  );

  public RelationshipTypeDescriptor DIRECTED = new RelationshipTypeDescriptor(
    "DIRECTED"
  );

  public RelationshipTypeDescriptor PRODUCED = new RelationshipTypeDescriptor(
    "PRODUCED"
  );

  public RelationshipTypeDescriptor WROTE = new RelationshipTypeDescriptor(
    "WROTE"
  );

  public RelationshipTypeDescriptor FOLLOWS = new RelationshipTypeDescriptor(
    "WROTE"
  );

  public RelationshipTypeDescriptor REVIEWED = new RelationshipTypeDescriptor(
    "REVIEWED"
  );
}
