package movieDescriptors.sampleProcedures;

import basics.relationship.RelationshipDescriptor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import movieDescriptors.MovieDbRelationshipTypes;
import movieDescriptors.node.*;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.procedure.Context;
import org.neo4j.procedure.Mode;
import org.neo4j.procedure.Name;
import org.neo4j.procedure.Procedure;

public class SampleProcedures {

  @Context
  public Transaction tx;

  public class NodeWrapper {

    public final Node Node;

    public NodeWrapper(Node node) {
      Node = node;
    }
  }

  @Procedure(mode = Mode.WRITE, name = "getPersonsYearBorn")
  public Stream<NodeWrapper> getPersonsYearBorn(
    @Name("bornAfter") long bornAfter
  ) {
    ArrayList<NodeWrapper> resultNodes = new ArrayList<>();
    /* All nodes returned in the next line are guaranteed to possess everything
     * declared in the PersonNodeDescriptor.
     * Well - tbf - the stuff not included in the NodeIdentifier is not absolutely
     * guaranteed, but assuming proper validation has been done recently, it
     * sorta is.*/
    Iterator<Node> personNodes = PersonNodeDescriptor
      .getInstance()
      .findNodes(tx)
      .iterator();
    String bornKey = PersonNodeDescriptor.getInstance().Born.dbKey();
    while (personNodes.hasNext()) {
      Node person = personNodes.next();
      long born = (long) person.getProperty(bornKey);
      if (born > bornAfter) {
        resultNodes.add(new NodeWrapper(person));
      }
    }
    return resultNodes.stream();
  }

  @Procedure(mode = Mode.WRITE, name = "setActedInMovieCount")
  public Stream<NodeWrapper> setActedInMovieCount() {
    ArrayList<NodeWrapper> resultNodes = new ArrayList<>();
    /* All nodes returned in the next line are guaranteed to possess everything
     * declared in the ActorNodeDescriptor.
     * Well - tbf - the stuff not included in the NodeIdentifier is not absolutely
     * guaranteed, but assuming proper validation has been done recently, it
     * sorta is.*/
    Iterator<Node> actorNodes = ActorNodeDescriptor
      .getInstance()
      .findNodes(tx)
      .iterator();
    while (actorNodes.hasNext()) {
      Node actor = actorNodes.next();
      RelationshipDescriptor actedRelDesc = ActorNodeDescriptor.getInstance()
        .ActedRelation;
      Iterable<Relationship> actedRelations = actor.getRelationships(
        actedRelDesc.direction(),
        actedRelDesc.relationshipType()
      );
      long movieCount = StreamSupport
        .stream(actedRelations.spliterator(), false)
        .count();
      actor.setProperty("movieCount", movieCount);
      resultNodes.add(new NodeWrapper(actor));
    }
    return resultNodes.stream();
  }

  @Procedure(mode = Mode.WRITE, name = "doLazyActionReviews")
  public void doLazyActionReviews(@Name("personNode") Node personNode) {
    if (!ReviewerNodeDescriptor.getInstance().isMatch(personNode)) {
      return;
    }
    Iterator<Node> actionMovieNode = ActionMovieNodeDescriptor
      .getInstance()
      .findNodes(tx)
      .iterator();
    while (actionMovieNode.hasNext()) {
      Node actionMovie = actionMovieNode.next();
      // Both the existence and the type of that Budget property are guaranteed by the specifications
      // in the ActionMovieNodeDescriptor class
      Number budget = ActionMovieNodeDescriptor
        .getInstance()
        .Budget.getValueOn(actionMovie);
      if (budget.doubleValue() > 25000000) {
        Relationship rel = personNode.createRelationshipTo(
          actionMovie,
          MovieDbRelationshipTypes.getInstance().REVIEWED.relationshipType()
        );
        rel.setProperty("rating", 96);
        rel.setProperty(
          "summary",
          "this was awesome and boy did they spend a ton of cash"
        );
      }
    }
  }
}
