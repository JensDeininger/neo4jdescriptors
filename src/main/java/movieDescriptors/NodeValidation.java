package movieDescriptors;

import basics.validation.*;
import java.util.stream.Stream;
import org.neo4j.graphdb.Transaction;
import org.neo4j.procedure.Context;
import org.neo4j.procedure.Mode;
import org.neo4j.procedure.Name;
import org.neo4j.procedure.Procedure;

public class NodeValidation {

  @Context
  public Transaction tx;

  @Procedure(mode = Mode.READ, name = "verifyFindNodesIsMatch")
  public Stream<MessageWrapper> verifyFindNodesIsMatch() {
    NodeValidationBase nodeVal = new NodeValidationBase(
      MovieDbNodes.getInstance()
    );
    return nodeVal.verifyFindNodesIsMatch(tx);
  }

  @Procedure(mode = Mode.READ, name = "verifyCypherFindNodes")
  public Stream<MessageWrapper> verifyCypherFindNodes() {
    NodeValidationBase nodeVal = new NodeValidationBase(
      MovieDbNodes.getInstance()
    );
    return nodeVal.verifyCypherFindNodes(tx);
  }

  @Procedure(mode = Mode.READ, name = "validateNodes")
  public Stream<MessageWrapper> validateNodes(
    @Name("minLevelName") String minLevelName
  ) {
    NodeValidationBase nodeVal = new NodeValidationBase(
      MovieDbNodes.getInstance()
    );
    return nodeVal.validateNodes(tx, minLevelName);
  }
}
