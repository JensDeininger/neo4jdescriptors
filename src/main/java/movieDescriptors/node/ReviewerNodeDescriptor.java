package movieDescriptors.node;

import basics.relationship.*;
import basics.validation.ValidationReport;
import movieDescriptors.MovieDbRelationshipTypes;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;

public class ReviewerNodeDescriptor extends PersonNodeDescriptor {

  // region singleton stuff

  private static ReviewerNodeDescriptor INSTANCE;

  public static ReviewerNodeDescriptor getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new ReviewerNodeDescriptor();
    }
    return INSTANCE;
  }

  // endregion singleton stuff

  private ReviewerNodeDescriptor() {
    /* The _nodeIdentifier describes the set of nodes that this class is supposed to describe.
     * Here, all we require is that the node matches the PersonDescriptor and that it has at
     * least one OUTGOING REVIEWED relation */
    _nodeIdentifier =
      super
        .getNodeIdentifier()
        .getOpenCopy()
        .requires(ReviewedRelation)
        .closeDefinition();
  }

  public RelationshipDescriptor ReviewedRelation = new RelationshipDescriptor(
    MovieDbRelationshipTypes.getInstance().REVIEWED,
    Direction.OUTGOING
  );

  /* The existence of this field ensures that during validation, every node matching the _nodeIdentifier
   * is checked for the existence of at least one OUTGOING REVIEWED relation to a node that
   * is described by the MovieNodeDescriptor class */
  public RelationshipNodePair<MovieNodeDescriptor> Relation2Movie = new RelationshipNodePair<MovieNodeDescriptor>(
    ReviewedRelation,
    MovieNodeDescriptor.class
  );

  @Override
  protected void specificValidation(Node node, ValidationReport report2Add2) {
    // Any specific validation beyond the standard ones one wants to perform can be done here
    // Any issues get reported in the ValidationReport as follows:
    // String msg = "Description of error";
    // report2Add2.append("Specific Validation", ValidationLevel.ERROR, msg);
  }
}
