package movieDescriptors.node;

import basics.relationship.*;
import basics.validation.*;
import movieDescriptors.MovieDbRelationshipTypes;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;

public class ActorNodeDescriptor extends PersonNodeDescriptor {

  // region singleton stuff

  private static ActorNodeDescriptor INSTANCE;

  public static ActorNodeDescriptor getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new ActorNodeDescriptor();
    }
    return INSTANCE;
  }

  // endregion singleton stuff

  private ActorNodeDescriptor() {
    /* The _nodeIdentifier describes the set of nodes that this class is supposed to describe.
     * Here, all we require is that the node matches the PersonDescriptor and that it has at
     * least one OUTGOING ACTED_IN relation */
    _nodeIdentifier =
      super
        .getNodeIdentifier()
        .getOpenCopy()
        .excludes(
          new RelationshipDescriptor(
            MovieDbRelationshipTypes.getInstance().DIRECTED
          )
        )
        .requires(ActedRelation)
        .closeDefinition();
  }

  public RelationshipDescriptor ActedRelation = new RelationshipDescriptor(
    MovieDbRelationshipTypes.getInstance().ACTED_IN,
    Direction.OUTGOING
  );

  /* The existence of this field ensures that during validation, every node matching the _nodeIdentifier
   * is checked for the existence of at least one OUTGOING ACTED_IN relation to a node that
   * is described by the MovieNodeDescriptor class */
  public RelationshipNodePair<MovieNodeDescriptor> Relation2Movie = new RelationshipNodePair<MovieNodeDescriptor>(
    ActedRelation,
    MovieNodeDescriptor.class
  );

  @Override
  protected void specificValidation(Node node, ValidationReport report2Add2) {
    // Any specific validation beyond the standard ones one wants to perform can be done here
    // Any issues get reported in the ValidationReport as follows:
    // String msg = "Description of error";
    // report2Add2.append("Specific Validation", ValidationLevel.ERROR, msg);
  }
}
