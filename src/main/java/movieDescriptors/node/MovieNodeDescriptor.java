package movieDescriptors.node;

import basics.NodeDescriptor;
import basics.property.*;
import basics.relationship.RelationshipDescriptor;
import basics.relationship.RelationshipNodePair;
import basics.validation.ValidationReport;
import movieDescriptors.MovieDbLabels;
import movieDescriptors.MovieDbRelationshipTypes;
import movieDescriptors.enumValue.*;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;

public class MovieNodeDescriptor extends NodeDescriptor {

  // region singleton stuff

  private static MovieNodeDescriptor INSTANCE;

  public static MovieNodeDescriptor getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new MovieNodeDescriptor();
    }
    return INSTANCE;
  }

  // endregion singleton stuff

  protected MovieNodeDescriptor() {
    /* The _nodeIdentifier describes the set of nodes that this class is supposed to describe.
     * Here, all we require is that the node has the Movie label */
    _nodeIdentifier =
      super
        .getNodeIdentifier()
        .getOpenCopy()
        .requires(MovieDbLabels.getInstance().Movie)
        .closeDefinition();
  }

  /* The existence of this field ensures that during validation, every node matching the _nodeIdentifier
   * is checked for the existence a property called 'released' of the type long */
  public LongProperty Released = new LongProperty("released");

  /* The existence of this field ensures that during validation, every node matching the _nodeIdentifier
   * is checked for the existence a property called 'tagline' of the type string */
  public StringProperty Tagline = new StringProperty("tagline");

  /* The existence of this field ensures that during validation, every node matching the _nodeIdentifier
   * is checked for the existence a property called 'title' of the type string */
  public StringProperty Title = new StringProperty("title");

  /* The existence of this field ensures that during validation, every node matching the _nodeIdentifier
   * is checked for the existence a property called 'genre'.
   * The type check is more elaborate than in the other cases. Only strings that are declared
   * in the Genre enum as valid are accepted */
  public StringEnumProperty<Genre> MovieGenre = new StringEnumProperty<>(
    "genre",
    Genre.class
  );

  /* The existence of this field ensures that during validation, every node matching the _nodeIdentifier
   * is checked for the existence of at least one INCOMING ACTED_IN relation to a node that
   * is described by the ActorNodeDescriptor class */
  public RelationshipNodePair<ActorNodeDescriptor> Relation2Actor = new RelationshipNodePair<>(
    new RelationshipDescriptor(
      MovieDbRelationshipTypes.getInstance().ACTED_IN,
      Direction.INCOMING
    ),
    ActorNodeDescriptor.class
  );

  @Override
  protected void specificValidation(Node node, ValidationReport report2Add2) {
    // Any specific validation beyond the standard ones one wants to perform can be done here
    // Any issues get reported in the ValidationReport as follows:
    // String msg = "Description of error";
    // report2Add2.append("Specific Validation", ValidationLevel.ERROR, msg);
  }
}
