package movieDescriptors.node;

import basics.*;
import basics.NodeDescriptor;
import basics.property.*;
import basics.validation.ValidationReport;
import movieDescriptors.MovieDbLabels;
import org.neo4j.graphdb.Node;

public class PersonNodeDescriptor extends NodeDescriptor {

  // region singleton stuff

  private static PersonNodeDescriptor INSTANCE;

  public static PersonNodeDescriptor getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new PersonNodeDescriptor();
    }
    return INSTANCE;
  }

  // endregion singleton stuff

  protected PersonNodeDescriptor() {
    /* The NodeIdetifier describes the set of Nodes that this
     * NodeDescriptor applies to.
     * Here, all we demand is that the node must have the Person label */

    _nodeIdentifier =
      super
        .getNodeIdentifier()
        .getOpenCopy()
        .requires(MovieDbLabels.getInstance().Person)
        .closeDefinition();
  }

  /* The PropertyDescriptors listed here can be used in user defined procedures
   * to supply the property keys as they are in the database. Should these ever
   * change, they need only be changed here.
   * In contrast to the components in the NodeDescriptor, any Descriptors here
   * do not serve to define the set of described nodes. Instead, their existence
   * is validated by the validation procedure. It is up to the user to call this
   * frequently and take approriate action in case of discrepancies. */

  public LongProperty Born = new LongProperty("born");

  public StringProperty Name = new StringProperty("name");

  @Override
  protected void specificValidation(Node node, ValidationReport report2Add2) {
    // Any specific validation beyond the standard ones one wants to perform can be done here
    // Any issues get reported in the ValidationReport as follows:
    // String msg = "Description of error";
    // report2Add2.append("Specific Validation", ValidationLevel.ERROR, msg);
  }
}
