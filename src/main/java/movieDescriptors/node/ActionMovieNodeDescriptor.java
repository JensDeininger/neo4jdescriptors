package movieDescriptors.node;

import basics.property.*;
import basics.property.StringPropertyValuePair;
import basics.validation.ValidationReport;
import movieDescriptors.enumValue.*;
import org.neo4j.graphdb.Node;

public class ActionMovieNodeDescriptor extends MovieNodeDescriptor {

  // region singleton stuff

  private static ActionMovieNodeDescriptor INSTANCE;

  public static ActionMovieNodeDescriptor getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new ActionMovieNodeDescriptor();
    }
    return INSTANCE;
  }

  // endregion singleton stuff

  protected ActionMovieNodeDescriptor() {
    /* The _nodeIdentifier describes the set of nodes that this class is supposed to describe.
     * Here, all we require is that the node matches the MovieDescriptor and that it
     * has a property named 'genre' whose value is 'action' or rather the value defined
     * in the enum Genre */
    _nodeIdentifier =
      super
        .getNodeIdentifier()
        .getOpenCopy()
        .requires(ActionGenre)
        .closeDefinition();
  }

  public StringEnumPropertyValuePair<Genre> ActionGenre = new StringEnumPropertyValuePair<>(
    this.MovieGenre,
    Genre.ACTION
  );

  /* The existence of this field ensures that during validation, every node matching the _nodeIdentifier
   * is checked for the existence a property called 'budget' of the type Float */
  public NumberProperty Budget = new NumberProperty("budget");

  @Override
  protected void specificValidation(Node node, ValidationReport report2Add2) {
    // Any specific validation beyond the standard ones one wants to perform can be done here
    // Any issues get reported in the ValidationReport as follows:
    // String msg = "Description of error";
    // report2Add2.append("Specific Validation", ValidationLevel.ERROR, msg);
  }
}
