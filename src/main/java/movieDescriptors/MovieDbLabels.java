/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movieDescriptors;

import basics.*;
import basics.LabelDescriptor;
import basics.repository.LabelDescriptorRepository;

public class MovieDbLabels extends LabelDescriptorRepository {

  // <stuff that can be copy pasted from a templates and will be the same whatever Database one wants to describe>

  private static MovieDbLabels INSTANCE;

  public static MovieDbLabels getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new MovieDbLabels();
    }
    return INSTANCE;
  }

  private MovieDbLabels() {
    super();
  }

  // </stuff that can be copy pasted from a templates and will be the same whatever Database one wants to describe>

  /* All LabelDescriptors one wishes to use must be listed here.
   * Since this class is a singleton, they will be available everywhere by calls like
   * MovieDbLabels.getInstance().Movie
   *
   * This class also inherits the method
   * public Stream<LabelDescriptor> labelDescriptors()
   * which will return all LabelDescriptors defined here.
   */

  public LabelDescriptor Movie = new LabelDescriptor("Movie");

  public LabelDescriptor Person = new LabelDescriptor("Person");
}
