package movieDescriptors;

import basics.repository.NodeDescriptorRepository;
import movieDescriptors.node.*;

public class MovieDbNodes extends NodeDescriptorRepository {

  // <stuff that can be copy pasted from a templates and will be the same whatever Database one wants to describe>

  private static MovieDbNodes INSTANCE;

  public static MovieDbNodes getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new MovieDbNodes();
    }
    return INSTANCE;
  }

  private MovieDbNodes() {}

  // </stuff that can be copy pasted from a templates and will be the same whatever Database one wants to describe>

  /* All NodeDescriptors one wishes to use must be listed here.
   * Since this class is a singleton, they will be available everywhere by calls like
   * MovieDbNodes.getInstance().Person
   *
   * This class also inherits the method   *
   * public Stream<NodeDescriptor> nodeDescriptors()
   * which will return all NodeDescriptorss defined here.
   */

  public PersonNodeDescriptor Person = PersonNodeDescriptor.getInstance();

  public ActorNodeDescriptor Actor = ActorNodeDescriptor.getInstance();

  public MovieNodeDescriptor Movie = MovieNodeDescriptor.getInstance();

  public MovieNodeDescriptor ActionMovie = ActionMovieNodeDescriptor.getInstance();

  public ReviewerNodeDescriptor Reviewer = ReviewerNodeDescriptor.getInstance();
}
