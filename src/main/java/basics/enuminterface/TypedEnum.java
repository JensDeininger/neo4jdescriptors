package basics.enuminterface;

public interface TypedEnum<T> {
  T dbValue();
}
