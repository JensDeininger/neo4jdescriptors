package basics.property;

import basics.validation.ValidationLevel;
import basics.validation.ValidationReport;
import org.neo4j.graphdb.Node;

public class LongProperty extends PropertyDescriptor<Long> {

  public LongProperty(String dbKey) {
    super(dbKey, Long.class);
  }

  public LongProperty(String dbKey, String eventKey) {
    super(dbKey, eventKey, Long.class);
  }

  @Override
  public PropertyType propertyType() {
    return PropertyType.Long;
  }

  @Override
  public LongProperty getCopy() {
    return new LongProperty(dbKey(), logKey());
  }

  @Override
  public boolean appliesTo(Node node) {
    if (!super.appliesTo(node)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    return propValue instanceof Long;
  }

  @Override
  public boolean validate(Node node, ValidationReport report2Append) {
    if (!super.validate(node, report2Append)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    if (!(propValue instanceof Long)) {
      String msg = String.format(
        "Property %s is of type %s. Expected type was: %s",
        this.dbKey(),
        propValue.getClass().getName(),
        Long.class
      );
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }
}
