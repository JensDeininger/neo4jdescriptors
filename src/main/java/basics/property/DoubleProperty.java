package basics.property;

import basics.validation.ValidationLevel;
import basics.validation.ValidationReport;
import org.neo4j.graphdb.Node;

public class DoubleProperty extends PropertyDescriptor<Double> {

  public DoubleProperty(String dbKey) {
    super(dbKey, Double.class);
  }

  public DoubleProperty(String dbKey, String eventKey) {
    super(dbKey, eventKey, Double.class);
  }

  @Override
  public PropertyType propertyType() {
    return PropertyType.Double;
  }

  @Override
  public DoubleProperty getCopy() {
    return new DoubleProperty(dbKey(), logKey());
  }

  @Override
  public boolean appliesTo(Node node) {
    if (!super.appliesTo(node)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    return propValue instanceof Double;
  }

  @Override
  public boolean validate(Node node, ValidationReport report2Append) {
    if (!super.validate(node, report2Append)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    if (!(propValue instanceof Double)) {
      String msg = String.format(
        "Property %s is of type %s. Expected type was: %s",
        this.dbKey(),
        propValue.getClass().getName(),
        Double.class
      );
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }
}
