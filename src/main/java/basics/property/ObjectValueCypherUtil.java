package basics.property;

import basics.enuminterface.*;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.regex.Pattern;
import org.apache.commons.lang3.NotImplementedException;

public class ObjectValueCypherUtil {

  private ObjectValueCypherUtil() {}

  public static String bool2Cypher(Boolean bool) {
    return Boolean.toString(bool);
  }

  public static String double2Cypher(Double number) {
    DecimalFormat df = new DecimalFormat(
      "#.#######################",
      new DecimalFormatSymbols(Locale.ROOT)
    );
    return df.format(number);
  }

  public static String doubleEnum2Cypher(DoubleEnum doubleEnum) {
    return double2Cypher(doubleEnum.dbValue());
  }

  public static String long2Cypher(Long number) {
    return Long.toString(number);
  }

  public static String longEnum2Cypher(LongEnum longEnum) {
    return long2Cypher(longEnum.dbValue());
  }

  public static String number2Cypher(Number number) {
    if (
      number instanceof BigInteger ||
      number instanceof Byte ||
      number instanceof Integer ||
      number instanceof Long
    ) {
      return long2Cypher(number.longValue());
    } else {
      return double2Cypher(number.doubleValue());
    }
  }

  public static String string2Cypher(String string) {
    Pattern SPECIAL_REGEX_CHARS = Pattern.compile("[{}()\\[\\].+*?^$\\\\|]");
    SPECIAL_REGEX_CHARS.matcher(string).replaceAll("\\\\$0");
    String s1 = SPECIAL_REGEX_CHARS.matcher(string).replaceAll("\\\\$0");
    return "'" + s1 + "'";
  }

  public static String stringEnum2Cypher(StringEnum stringEnum) {
    return string2Cypher(stringEnum.dbValue());
  }

  public static String object2Cypher(Object object) {
    if (object == null) {
      return "";
    } else if (object instanceof Boolean) {
      return bool2Cypher((boolean) object);
    } else if (object instanceof Double) {
      return double2Cypher((Double) object);
    } else if (object instanceof DoubleEnum) {
      return doubleEnum2Cypher((DoubleEnum) object);
    } else if (object instanceof Long) {
      return long2Cypher((Long) object);
    } else if (object instanceof LongEnum) {
      return longEnum2Cypher((LongEnum) object);
    } else if (object instanceof Number) {
      return number2Cypher((Number) object);
    } else if (object instanceof String) {
      return string2Cypher((String) object);
    } else if (object instanceof StringEnum) {
      return stringEnum2Cypher((StringEnum) object);
    } else {
      throw new NotImplementedException();
    }
  }
}
