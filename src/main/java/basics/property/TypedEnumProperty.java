package basics.property;

import basics.enuminterface.TypedEnum;
import basics.validation.ValidationLevel;
import basics.validation.ValidationReport;
import java.util.Arrays;
import java.util.Optional;
import org.neo4j.graphdb.Node;

/* I have no idea why I cant use this as a base class and derive something like LongEnumProperty from it
 */

public abstract class TypedEnumProperty<T extends Enum<T> & TypedEnum<T>>
  extends PropertyDescriptor<T> {

  public TypedEnumProperty(String dbKey, Class<T> enumType) {
    super(dbKey, enumType);
  }

  public TypedEnumProperty(String dbKey, String logKey, Class<T> enumType) {
    super(dbKey, logKey, enumType);
  }

  @Override
  public abstract PropertyType propertyType();

  @Override
  public abstract TypedEnumProperty<T> getCopy();

  protected abstract boolean valuesAreEqual(T v1, T v2);

  private T tryTranslate(T dbValueFound) {
    T[] values = genericType().getEnumConstants();
    Optional<T> matchingEnumValue = Arrays
      .stream(values)
      .filter(o -> valuesAreEqual(o.dbValue(), dbValueFound))
      .findFirst();
    if (matchingEnumValue.isPresent()) {
      return matchingEnumValue.get();
    }
    return null;
  }

  @Override
  public boolean appliesTo(Node node) {
    if (!super.appliesTo(node)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    if (!this.genericType().isInstance(propValue)) {
      return false;
    }
    T dbValueFound = this.genericType().cast(propValue);
    return tryTranslate(dbValueFound) != null;
  }

  @Override
  public boolean validate(Node node, ValidationReport report2Append) {
    if (!super.validate(node, report2Append)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    if (!this.genericType().isInstance(propValue)) {
      String msg = String.format(
        "Property %s is of type %s. Expected type was: %s",
        this.dbKey(),
        propValue.getClass().getName(),
        genericType()
      );
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    T dbValueFound = this.genericType().cast(propValue);
    if (tryTranslate(dbValueFound) == null) {
      String msg = String.format(
        "%s is not a valid enum value for %s",
        dbValueFound,
        genericType()
      );
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }

  @Override
  public T getValueOn(org.neo4j.graphdb.Node node) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    if (!node.hasProperty(this.dbKey())) {
      throw new IllegalArgumentException(
        "node does not have a property named " + this.dbKey()
      );
    }
    Object propValue = node.getProperty(this.dbKey());
    if (!this.genericType().isInstance(propValue)) {
      String msg = String.format(
        "Property %s on Node<%d> (MATCH (n) WHERE ID(n) = %d RETURN n) is of type %s, expected type was %s",
        this.dbKey(),
        node.getId(),
        node.getId(),
        propValue == null ? "null" : propValue.getClass(),
        genericType()
      );
      throw new ClassCastException(msg);
    }
    T dbValueFound = this.genericType().cast(propValue);
    T enumValue = tryTranslate(dbValueFound);
    if (enumValue == null) {
      String msg = String.format(
        "%s is not a valid enum value for %s",
        dbValueFound,
        genericType()
      );
      throw new ClassCastException(msg);
    }
    return enumValue;
  }

  @Override
  public void setValueOn(org.neo4j.graphdb.Node node, T value) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    node.setProperty(this.dbKey(), value.dbValue());
  }
}
