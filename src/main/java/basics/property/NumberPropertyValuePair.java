package basics.property;

import java.util.Objects;

public class NumberPropertyValuePair extends PropertyValuePair<Number> {

  private int _hash;

  public NumberPropertyValuePair(NumberProperty property, Number value) {
    super(property, value);
    _hash = getProperty().hashCode();
    _hash = (_hash * 397) ^ getValue().hashCode();
  }

  @Override
  public NumberProperty getProperty() {
    return (NumberProperty) super.getProperty();
  }

  @Override
  public NumberPropertyValuePair getCopy() {
    return new NumberPropertyValuePair(getProperty().getCopy(), getValue());
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    NumberPropertyValuePair cast = NumberPropertyValuePair.class.cast(that);
    if (!getProperty().equals(cast.getProperty())) {
      return false;
    }
    return Objects.equals(this.getValue(), cast.getValue());
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
