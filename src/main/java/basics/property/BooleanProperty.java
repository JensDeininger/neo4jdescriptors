package basics.property;

import basics.validation.ValidationLevel;
import basics.validation.ValidationReport;
import org.neo4j.graphdb.Node;

public class BooleanProperty extends PropertyDescriptor<Boolean> {

  public BooleanProperty(String dbKey) {
    super(dbKey, Boolean.class);
  }

  public BooleanProperty(String dbKey, String logKey) {
    super(dbKey, logKey, Boolean.class);
  }

  @Override
  public PropertyType propertyType() {
    return PropertyType.Boolean;
  }

  @Override
  public BooleanProperty getCopy() {
    return new BooleanProperty(dbKey(), logKey());
  }

  @Override
  public boolean appliesTo(Node node) {
    if (!super.appliesTo(node)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    return propValue instanceof Boolean;
  }

  @Override
  public boolean validate(Node node, ValidationReport report2Append) {
    if (!super.validate(node, report2Append)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    if (!(propValue instanceof Boolean)) {
      String msg = String.format(
        "Property %s is of type %s. Expected type was: %s",
        this.dbKey(),
        propValue.getClass().getName(),
        Boolean.class
      );
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }
}
