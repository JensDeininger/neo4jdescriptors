package basics.property;

import basics.validation.ValidationLevel;
import basics.validation.ValidationReport;
import java.math.BigInteger;
import org.neo4j.graphdb.Node;

public class NumberProperty extends PropertyDescriptor<Number> {

  public NumberProperty(String dbKey) {
    super(dbKey, Number.class);
  }

  public NumberProperty(String dbKey, String eventKey) {
    super(dbKey, eventKey, Number.class);
  }

  @Override
  public PropertyType propertyType() {
    return PropertyType.Number;
  }

  @Override
  public NumberProperty getCopy() {
    return new NumberProperty(dbKey(), logKey());
  }

  @Override
  public boolean appliesTo(Node node) {
    if (!super.appliesTo(node)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    return propValue instanceof Long || propValue instanceof Double;
  }

  @Override
  public boolean validate(Node node, ValidationReport report2Append) {
    if (!super.validate(node, report2Append)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    if (!(propValue instanceof Long || propValue instanceof Double)) {
      String msg = String.format(
        "Property %s is of type %s. Expected type was: %s",
        this.dbKey(),
        propValue.getClass().getName(),
        Long.class
      );
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }

  @Override
  public Number getValueOn(org.neo4j.graphdb.Node node) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    if (!node.hasProperty(this.dbKey())) {
      throw new IllegalArgumentException(
        "node does not have a property named " + this.dbKey()
      );
    }
    Object value = node.getProperty(this.dbKey());
    if (
      !(
        value instanceof Double ||
        value instanceof Long ||
        value instanceof Number
      )
    ) {
      String msg = String.format(
        "Property %s on Node<%d> ( MATCH (n) WHERE ID(n) = %d RETURN n ) is of type %s, expected type was %s or %s",
        this.dbKey(),
        node.getId(),
        node.getId(),
        value.getClass(),
        Double.class,
        Long.class
      );
      throw new ClassCastException(msg);
    }
    Number num = (Number) value;
    if (value instanceof Long) {
      return num.longValue();
    } else {
      return num.doubleValue();
    }
  }

  @Override
  public void setValueOn(org.neo4j.graphdb.Node node, Number value) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    if (
      value instanceof BigInteger ||
      value instanceof Byte ||
      value instanceof Integer ||
      value instanceof Long
    ) {
      node.setProperty(this.dbKey(), value.longValue());
    } else {
      node.setProperty(this.dbKey(), value.doubleValue());
    }
  }
}
