package basics.property;

import java.util.Objects;

public class BoolPropertyValuePair extends PropertyValuePair<Boolean> {

  private int _hash;

  public BoolPropertyValuePair(BooleanProperty property, boolean value) {
    super(property, value);
    _hash = getProperty().hashCode();
    _hash = (_hash * 397) ^ Boolean.hashCode(getValue());
  }

  @Override
  public BooleanProperty getProperty() {
    return (BooleanProperty) super.getProperty();
  }

  @Override
  public BoolPropertyValuePair getCopy() {
    return new BoolPropertyValuePair(getProperty().getCopy(), getValue());
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    BoolPropertyValuePair cast = BoolPropertyValuePair.class.cast(that);
    if (!getProperty().equals(cast.getProperty())) {
      return false;
    }
    return Objects.equals(this.getValue(), cast.getValue());
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
