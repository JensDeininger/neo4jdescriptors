package basics.property;

import java.util.Objects;

public class LongPropertyValuePair extends PropertyValuePair<Long> {

  private int _hash;

  public LongPropertyValuePair(LongProperty property, long value) {
    super(property, value);
    _hash = getProperty().hashCode();
    _hash = (_hash * 397) ^ Long.hashCode(getValue());
  }

  @Override
  public LongProperty getProperty() {
    return (LongProperty) super.getProperty();
  }

  @Override
  public LongPropertyValuePair getCopy() {
    return new LongPropertyValuePair(getProperty().getCopy(), getValue());
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    LongPropertyValuePair cast = LongPropertyValuePair.class.cast(that);
    if (!getProperty().equals(cast.getProperty())) {
      return false;
    }
    return Objects.equals(this.getValue(), cast.getValue());
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
