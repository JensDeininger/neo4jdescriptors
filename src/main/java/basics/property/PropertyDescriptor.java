package basics.property;

import basics.validation.ValidationLevel;
import basics.validation.ValidationReport;
import java.util.HashMap;
import java.util.HashSet;
import org.neo4j.graphdb.Node;

/**
 * A {@link PropertyDescriptor} describes a property in the database.
 * <p>
 * It is fully defined by two {@link String}s, the dbName and the logName, both of
 * which must be specified in the constructor.
 * <p>
 * The dbName must be the name of the Label in the database.
 * <p>
 * The logName is optional and will be the same as the dbName if not specified in
 * the constructor. It can be used to record database operations in logs or archives
 * so that old records are not affected by changes to the Label name in the database.
 * <p>
 * {@link PropertyDescriptor} serves as the abstract base class to all typed subclasses
 * that further specify the {@link PropertyType} of the property.
 * <p>
 * {@link PropertyDescriptor} overrides {@link #equals() equals()} and
 * {@link #hashCode() hashCode()} so that identity is determined only by the name(s).
 * <p>
 * A {@link PropertyDescriptor} is immutable, so it can safely be used in {@link HashSet},
 * {@link HashMap} and the like.
 */
public abstract class PropertyDescriptor<T> {

  private int _hash;

  private String _dbKey;

  private String _logKey;

  private final Class<T> _type;

  protected PropertyDescriptor(String dbKey, Class<T> type) {
    this(dbKey, dbKey, type);
  }

  protected PropertyDescriptor(String dbKey, String logKey, Class<T> type) {
    if (dbKey == null) {
      throw new IllegalArgumentException("dbKey can not be null");
    }
    if (logKey == null) {
      throw new IllegalArgumentException("eventKey can not be null");
    }
    this._dbKey = dbKey;
    this._logKey = logKey;
    this._type = type;
    _hash = _dbKey.hashCode();
    _hash = (_hash * 397) ^ _logKey.hashCode();
    _hash = (_hash * 397) ^ _type.hashCode();
  }

  protected Class<T> genericType() {
    return _type;
  }

  public String dbKey() {
    return _dbKey;
  }

  public String logKey() {
    return _logKey;
  }

  public abstract PropertyType propertyType();

  public abstract PropertyDescriptor<T> getCopy();

  /**
   * Only checks if the specified node has the expected property key
   * @param node
   * @return
   */
  public boolean appliesTo(Node node) {
    return node.hasProperty(this.dbKey());
  }

  public boolean validate(Node node, ValidationReport report2Append) {
    if (!node.hasProperty(this.dbKey())) {
      String msg = String.format("Node lacks property key %s", this.dbKey());
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }

  public T getValueOn(org.neo4j.graphdb.Node node) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    if (!node.hasProperty(this.dbKey())) {
      throw new IllegalArgumentException(
        "node does not have a property named " + this.dbKey()
      );
    }
    Object nodeValue = node.getProperty(this.dbKey());
    if (_type.isInstance(nodeValue)) {
      String msg = String.format(
        "Property %s on Node<%d> (MATCH (n) WHERE ID(n) = %d RETURN n) is of type %s, expected type was %s",
        this.dbKey(),
        node.getId(),
        node.getId(),
        nodeValue.getClass(),
        _type
      );
      throw new ClassCastException(msg);
    }
    return _type.cast(nodeValue);
  }

  public void setValueOn(org.neo4j.graphdb.Node node, T value) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    node.setProperty(this.dbKey(), value);
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    PropertyDescriptor<?> cast = (PropertyDescriptor<?>) that;
    if (!this._type.equals(cast._type)) {
      return false;
    }
    if (!_dbKey.equals(cast._dbKey)) {
      return false;
    }
    return _logKey.equals(cast._logKey);
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
