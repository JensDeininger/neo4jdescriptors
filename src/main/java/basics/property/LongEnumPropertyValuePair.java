package basics.property;

import basics.enuminterface.LongEnum;
import java.util.Objects;

public class LongEnumPropertyValuePair<T extends Enum<T> & LongEnum>
  extends PropertyValuePair<T> {

  private int _hash;

  public LongEnumPropertyValuePair(LongEnumProperty<T> property, T value) {
    super(property, value);
    _hash = getProperty().hashCode();
    _hash = (_hash * 397) ^ value.hashCode();
  }

  @Override
  public LongEnumProperty<T> getProperty() {
    return (LongEnumProperty<T>) super.getProperty();
  }

  @Override
  public LongEnumPropertyValuePair<T> getCopy() {
    return new LongEnumPropertyValuePair<>(getProperty().getCopy(), getValue());
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    LongEnumPropertyValuePair<?> cast = this.getClass().cast(that);
    if (!getProperty().equals(cast.getProperty())) {
      return false;
    }
    return Objects.equals(this.getValue(), cast.getValue());
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
