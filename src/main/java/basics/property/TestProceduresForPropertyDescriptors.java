package basics.property;

import org.neo4j.graphdb.Node;
import org.neo4j.procedure.Mode;
import org.neo4j.procedure.Name;
import org.neo4j.procedure.Procedure;

/**
 * These procedures exist only to test the getValueOn method in PropertyDescriptor
 */
public class TestProceduresForPropertyDescriptors {

  @Procedure(
    mode = Mode.WRITE,
    name = "testProcedure_BooleanProperty_getValueOn"
  )
  public void testProcedure_BooleanProperty_getValueOn(
    @Name("targetNode") Node targetNode
  ) {
    String dbKey = "foobar";
    boolean expectedValue = true;
    targetNode.setProperty(dbKey, expectedValue);
    BooleanProperty prop = new BooleanProperty(dbKey);
    if (!prop.appliesTo(targetNode)) {
      throw new RuntimeException("Fail BooleanProperty.appliesTo");
    }
    boolean actualValue = prop.getValueOn(targetNode);
    if (actualValue != expectedValue) {
      throw new RuntimeException("Fail BooleanProperty.getValueOn");
    }
  }

  @Procedure(mode = Mode.WRITE, name = "testProcedure_FloatProperty_getValueOn")
  public void testProcedure_FloatProperty_getValueOn(
    @Name("targetNode") Node targetNode
  ) {
    String dbKey = "foobar";
    double expectedValue = 3.14;
    targetNode.setProperty(dbKey, expectedValue);
    DoubleProperty prop = new DoubleProperty(dbKey);
    if (!prop.appliesTo(targetNode)) {
      throw new RuntimeException("Fail FloatProperty.appliesTo");
    }
    double actualValue = prop.getValueOn(targetNode);
    if (actualValue != expectedValue) {
      throw new RuntimeException("Fail FloatProperty.getValueOn");
    }
  }

  @Procedure(mode = Mode.WRITE, name = "testProcedure_LongProperty_getValueOn")
  public void testProcedure_LongProperty_getValueOn(
    @Name("targetNode") Node targetNode
  ) {
    String dbKey = "foobar";
    long expectedValue = 23;
    targetNode.setProperty(dbKey, expectedValue);
    LongProperty prop = new LongProperty(dbKey);
    if (!prop.appliesTo(targetNode)) {
      throw new RuntimeException("Fail LongProperty.appliesTo");
    }
    long actualValue = prop.getValueOn(targetNode);
    if (actualValue != expectedValue) {
      throw new RuntimeException("Fail LongProperty.getValueOn");
    }
  }

  @Procedure(
    mode = Mode.WRITE,
    name = "testProcedure_NumberProperty_getValueOn"
  )
  public void testProcedure_NumberProperty_getValueOn(
    @Name("targetNode") Node targetNode
  ) {
    String dbKey1 = "foo";
    long expectedValue1 = 3;
    targetNode.setProperty(dbKey1, expectedValue1);
    NumberProperty prop1 = new NumberProperty(dbKey1);
    if (!prop1.appliesTo(targetNode)) {
      throw new RuntimeException("Fail 1 NumberProperty.appliesTo");
    }
    Number actualValue1 = prop1.getValueOn(targetNode);
    if (!actualValue1.equals(expectedValue1)) {
      throw new RuntimeException("Fail 1 LongProperty.getValueOn");
    }

    String dbKey2 = "bar";
    double expectedValue2 = Math.PI;
    targetNode.setProperty(dbKey2, expectedValue2);
    NumberProperty prop2 = new NumberProperty(dbKey2);
    if (!prop1.appliesTo(targetNode)) {
      throw new RuntimeException("Fail 2 NumberProperty.appliesTo");
    }
    Number actualValue2 = prop2.getValueOn(targetNode);
    if (!actualValue2.equals(expectedValue2)) {
      throw new RuntimeException("Fail 2 LongProperty.getValueOn");
    }
  }

  @Procedure(
    mode = Mode.WRITE,
    name = "testProcedure_StringProperty_getValueOn"
  )
  public void testProcedure_StringProperty_getValueOn(
    @Name("targetNode") Node targetNode
  ) {
    String dbKey = "foobar";
    String expectedValue = "whoopsie";
    targetNode.setProperty(dbKey, expectedValue);
    StringProperty prop = new StringProperty(dbKey);
    if (!prop.appliesTo(targetNode)) {
      throw new RuntimeException("Fail StringProperty.appliesTo");
    }
    String actualValue = prop.getValueOn(targetNode);
    if (!actualValue.equals(expectedValue)) {
      throw new RuntimeException("Fail StringProperty.getValueOn");
    }
  }
}
