package basics.property;

import basics.enuminterface.DoubleEnum;
import java.util.Objects;

public class DoubleEnumPropertyValuePair<T extends Enum<T> & DoubleEnum>
  extends PropertyValuePair<T> {

  private int _hash;

  public DoubleEnumPropertyValuePair(DoubleEnumProperty<T> property, T value) {
    super(property, value);
    _hash = getProperty().hashCode();
    _hash = (_hash * 397) ^ value.hashCode();
  }

  @Override
  public DoubleEnumProperty<T> getProperty() {
    return (DoubleEnumProperty<T>) super.getProperty();
  }

  @Override
  public DoubleEnumPropertyValuePair<T> getCopy() {
    return new DoubleEnumPropertyValuePair<>(
      getProperty().getCopy(),
      getValue()
    );
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    DoubleEnumPropertyValuePair<?> cast = this.getClass().cast(that);
    if (!getProperty().equals(cast.getProperty())) {
      return false;
    }
    return Objects.equals(this.getValue(), cast.getValue());
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
