package basics.property;

import basics.enuminterface.StringEnum;
import java.util.Objects;

public class StringEnumPropertyValuePair<T extends Enum<T> & StringEnum>
  extends PropertyValuePair<T> {

  private int _hash;

  public StringEnumPropertyValuePair(StringEnumProperty<T> property, T value) {
    super(property, value);
    _hash = getProperty().hashCode();
    _hash = (_hash * 397) ^ value.hashCode();
  }

  @Override
  public StringEnumProperty<T> getProperty() {
    return (StringEnumProperty<T>) super.getProperty();
  }

  @Override
  public StringEnumPropertyValuePair<T> getCopy() {
    return new StringEnumPropertyValuePair<>(
      getProperty().getCopy(),
      getValue()
    );
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    StringEnumPropertyValuePair<?> cast = this.getClass().cast(that);
    if (!getProperty().equals(cast.getProperty())) {
      return false;
    }
    return Objects.equals(this.getValue(), cast.getValue());
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
