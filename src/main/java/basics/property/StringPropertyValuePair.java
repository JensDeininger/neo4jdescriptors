package basics.property;

import java.util.Objects;

public class StringPropertyValuePair extends PropertyValuePair<String> {

  private int _hash;

  public StringPropertyValuePair(StringProperty property, String value) {
    super(property, value);
    _hash = getProperty().hashCode();
    _hash = (_hash * 397) ^ getValue().hashCode();
  }

  @Override
  public StringProperty getProperty() {
    return (StringProperty) super.getProperty();
  }

  @Override
  public StringPropertyValuePair getCopy() {
    return new StringPropertyValuePair(getProperty().getCopy(), getValue());
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    StringPropertyValuePair cast = StringPropertyValuePair.class.cast(that);
    if (!getProperty().equals(cast.getProperty())) {
      return false;
    }
    return Objects.equals(this.getValue(), cast.getValue());
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
