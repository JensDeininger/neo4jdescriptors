package basics.property;

import basics.validation.ValidationLevel;
import basics.validation.ValidationReport;
import org.neo4j.graphdb.Node;

public class StringProperty extends PropertyDescriptor<String> {

  public StringProperty(String dbKey) {
    super(dbKey, String.class);
  }

  public StringProperty(String dbKey, String eventKey) {
    super(dbKey, eventKey, String.class);
  }

  @Override
  public PropertyType propertyType() {
    return PropertyType.String;
  }

  @Override
  public StringProperty getCopy() {
    return new StringProperty(dbKey(), logKey());
  }

  @Override
  public boolean appliesTo(Node node) {
    if (!super.appliesTo(node)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    return propValue instanceof String;
  }

  @Override
  public boolean validate(Node node, ValidationReport report2Append) {
    if (!super.validate(node, report2Append)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    if (!(propValue instanceof String)) {
      String msg = String.format(
        "Property %s is of type %s. Expected type was: %s",
        this.dbKey(),
        propValue.getClass().getName(),
        Long.class
      );
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }
}
