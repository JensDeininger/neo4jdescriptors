package basics.property;

import basics.validation.ValidationReport;
import org.neo4j.graphdb.Node;

public class ObjectProperty extends PropertyDescriptor<Object> {

  public ObjectProperty(String dbKey) {
    super(dbKey, Object.class);
  }

  public ObjectProperty(String dbKey, String logKey) {
    super(dbKey, logKey, Object.class);
  }

  @Override
  public PropertyType propertyType() {
    return PropertyType.Any;
  }

  @Override
  public ObjectProperty getCopy() {
    return new ObjectProperty(dbKey(), logKey());
  }

  @Override
  public Object getValueOn(org.neo4j.graphdb.Node node) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    if (!node.hasProperty(this.dbKey())) {
      throw new IllegalArgumentException(
        "node does not have a property named " + this.dbKey()
      );
    }
    Object value = node.getProperty(this.dbKey());
    return value;
  }

  @Override
  public void setValueOn(org.neo4j.graphdb.Node node, Object value) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    node.setProperty(this.dbKey(), value);
  }

  @Override
  public boolean validate(Node node, ValidationReport report2Append) {
    if (!super.validate(node, report2Append)) {
      return false;
    }
    return true;
  }
}
