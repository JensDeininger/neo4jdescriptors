package basics.property;

import java.util.Objects;

public class DoublePropertyValuePair extends PropertyValuePair<Double> {

  private int _hash;

  public DoublePropertyValuePair(DoubleProperty property, double value) {
    super(property, value);
    _hash = getProperty().hashCode();
    _hash = (_hash * 397) ^ Double.hashCode(getValue());
  }

  @Override
  public DoubleProperty getProperty() {
    return (DoubleProperty) super.getProperty();
  }

  @Override
  public DoublePropertyValuePair getCopy() {
    return new DoublePropertyValuePair(getProperty().getCopy(), getValue());
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    DoublePropertyValuePair cast = DoublePropertyValuePair.class.cast(that);
    if (!getProperty().equals(cast.getProperty())) {
      return false;
    }
    return Objects.equals(this.getValue(), cast.getValue());
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
