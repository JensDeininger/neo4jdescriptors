package basics.property;

import basics.enuminterface.DoubleEnum;
import basics.validation.ValidationLevel;
import basics.validation.ValidationReport;
import java.util.Arrays;
import java.util.Optional;
import org.neo4j.graphdb.Node;

public class DoubleEnumProperty<T extends Enum<T> & DoubleEnum>
  extends PropertyDescriptor<T> {

  public DoubleEnumProperty(String dbKey, Class<T> enumType) {
    super(dbKey, enumType);
  }

  public DoubleEnumProperty(String dbKey, String logKey, Class<T> enumType) {
    super(dbKey, logKey, enumType);
  }

  @Override
  public PropertyType propertyType() {
    return PropertyType.DoubleEnum;
  }

  @Override
  public DoubleEnumProperty<T> getCopy() {
    return new DoubleEnumProperty<>(dbKey(), logKey(), genericType());
  }

  private T tryTranslate(Double dbValueFound) {
    T[] values = genericType().getEnumConstants();
    Optional<T> matchingEnumValue = Arrays
      .stream(values)
      .filter(o -> Math.abs(o.dbValue() - dbValueFound) < 0.000001)
      .findFirst();
    if (matchingEnumValue.isPresent()) {
      return matchingEnumValue.get();
    }
    return null;
  }

  @Override
  public boolean appliesTo(Node node) {
    if (!super.appliesTo(node)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    if (!(propValue instanceof Double)) {
      return false;
    }
    Double dbValueFound = (Double) propValue;
    return tryTranslate(dbValueFound) != null;
  }

  @Override
  public boolean validate(Node node, ValidationReport report2Append) {
    if (!super.validate(node, report2Append)) {
      return false;
    }
    Object propValue = node.getProperty(this.dbKey());
    if (!(propValue instanceof Double)) {
      String msg = String.format(
        "Property %s is of type %s. Expected type was: %s",
        this.dbKey(),
        propValue.getClass().getName(),
        genericType()
      );
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    Double dbValueFound = (Double) propValue;
    if (tryTranslate(dbValueFound) == null) {
      String msg = String.format(
        "%s is not a valid enum value for %s",
        dbValueFound,
        genericType()
      );
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }

  @Override
  public T getValueOn(org.neo4j.graphdb.Node node) {
    super.getValueOn(node);
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    if (!node.hasProperty(this.dbKey())) {
      throw new IllegalArgumentException(
        "node does not have a property named " + this.dbKey()
      );
    }
    Object propValue = node.getProperty(this.dbKey());
    if (propValue == null || !propValue.getClass().equals(Double.class)) {
      String msg = String.format(
        "Property %s on Node<%d> (MATCH (n) WHERE ID(n) = %d RETURN n) is of type %s, expected type was %s",
        this.dbKey(),
        node.getId(),
        node.getId(),
        propValue == null ? "null" : propValue.getClass(),
        genericType()
      );
      throw new ClassCastException(msg);
    }
    Double dbValueFound = (Double) propValue;
    T enumValue = tryTranslate(dbValueFound);
    if (enumValue == null) {
      String msg = String.format(
        "%s is not a valid enum value for %s",
        dbValueFound,
        genericType()
      );
      throw new ClassCastException(msg);
    }
    return enumValue;
  }

  @Override
  public void setValueOn(org.neo4j.graphdb.Node node, T value) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    node.setProperty(this.dbKey(), value.dbValue());
  }
}
