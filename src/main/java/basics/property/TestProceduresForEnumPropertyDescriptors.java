package basics.property;

import basics.enuminterface.*;
import org.neo4j.graphdb.Node;
import org.neo4j.procedure.Mode;
import org.neo4j.procedure.Name;
import org.neo4j.procedure.Procedure;

/**
 * These procedures exist only to test the getValueOn method in PropertyDescriptor
 */
public class TestProceduresForEnumPropertyDescriptors {

  enum DoubleDays implements DoubleEnum {
    MONDAY(1.0),
    TUESDAY(2.0),
    WEDNESDAY(3.0),
    THURSDAY(4.0),
    FRIDAY(5.0),
    SATURDAY(6.0),
    SUNDAY(7.0);

    private final Double _dbValue;

    DoubleDays(Double dbValue) {
      this._dbValue = dbValue;
    }

    public Double dbValue() {
      return _dbValue;
    }
  }

  class DoubleDaysEnumProperty extends DoubleEnumProperty<DoubleDays> {

    public DoubleDaysEnumProperty(String dbKey) {
      super(dbKey, DoubleDays.class);
    }

    public DoubleDaysEnumProperty(String dbKey, String logKey) {
      super(dbKey, logKey, DoubleDays.class);
    }

    @Override
    public DoubleDaysEnumProperty getCopy() {
      return new DoubleDaysEnumProperty(dbKey(), logKey());
    }
  }

  @Procedure(
    mode = Mode.WRITE,
    name = "testProcedure_DoubleEnumProperty_getValueOn"
  )
  public void testProcedure_DoubleEnumProperty_getValueOn(
    @Name("targetNode") Node targetNode
  ) {
    String dbKey = "foobar";
    DoubleDays expectedValue = DoubleDays.THURSDAY;
    targetNode.setProperty(dbKey, expectedValue.dbValue());
    DoubleDaysEnumProperty prop = new DoubleDaysEnumProperty(dbKey);
    if (!prop.appliesTo(targetNode)) {
      throw new RuntimeException("Fail StringEnumProperty.appliesTo");
    }
    DoubleDays actualValue = prop.getValueOn(targetNode);
    if (!actualValue.equals(expectedValue)) {
      throw new RuntimeException("Fail StringEnumProperty.getValueOn");
    }
  }

  /********************************************************************/

  enum LongDays implements LongEnum {
    MONDAY(1l),
    TUESDAY(2l),
    WEDNESDAY(3l),
    THURSDAY(4l),
    FRIDAY(5l),
    SATURDAY(6l),
    SUNDAY(7l);

    private final Long _dbValue;

    LongDays(Long dbValue) {
      this._dbValue = dbValue;
    }

    public Long dbValue() {
      return _dbValue;
    }
  }

  class LongDaysEnumProperty extends LongEnumProperty<LongDays> {

    public LongDaysEnumProperty(String dbKey) {
      super(dbKey, LongDays.class);
    }

    public LongDaysEnumProperty(String dbKey, String logKey) {
      super(dbKey, logKey, LongDays.class);
    }

    @Override
    public LongDaysEnumProperty getCopy() {
      return new LongDaysEnumProperty(dbKey(), logKey());
    }
  }

  @Procedure(
    mode = Mode.WRITE,
    name = "testProcedure_LongEnumProperty_getValueOn"
  )
  public void testProcedure_LongEnumProperty_getValueOn(
    @Name("targetNode") Node targetNode
  ) {
    String dbKey = "foobar";
    LongDays expectedValue = LongDays.THURSDAY;
    targetNode.setProperty(dbKey, expectedValue.dbValue());
    LongDaysEnumProperty prop = new LongDaysEnumProperty(dbKey);
    if (!prop.appliesTo(targetNode)) {
      throw new RuntimeException("Fail StringEnumProperty.appliesTo");
    }
    LongDays actualValue = prop.getValueOn(targetNode);
    if (!actualValue.equals(expectedValue)) {
      throw new RuntimeException("Fail StringEnumProperty.getValueOn");
    }
  }

  /********************************************************************/

  enum StringDays implements StringEnum {
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    SUNDAY("Sunday");

    private final String _dbValue;

    StringDays(String dbValue) {
      this._dbValue = dbValue;
    }

    public String dbValue() {
      return _dbValue;
    }
  }

  class StringDaysEnumProperty extends StringEnumProperty<StringDays> {

    public StringDaysEnumProperty(String dbKey) {
      super(dbKey, StringDays.class);
    }

    public StringDaysEnumProperty(String dbKey, String logKey) {
      super(dbKey, logKey, StringDays.class);
    }

    @Override
    public StringDaysEnumProperty getCopy() {
      return new StringDaysEnumProperty(dbKey(), logKey());
    }
  }

  @Procedure(
    mode = Mode.WRITE,
    name = "testProcedure_StringEnumProperty_getValueOn"
  )
  public void testProcedure_StringEnumProperty_getValueOn(
    @Name("targetNode") Node targetNode
  ) {
    String dbKey = "foobar";
    StringDays expectedValue = StringDays.THURSDAY;
    targetNode.setProperty(dbKey, expectedValue.dbValue());
    StringDaysEnumProperty prop = new StringDaysEnumProperty(dbKey);
    if (!prop.appliesTo(targetNode)) {
      throw new RuntimeException("Fail StringEnumProperty.appliesTo");
    }
    StringDays actualValue = prop.getValueOn(targetNode);
    if (!actualValue.equals(expectedValue)) {
      throw new RuntimeException("Fail StringEnumProperty.getValueOn");
    }
  }
}
