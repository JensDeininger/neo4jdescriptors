package basics.property;

import basics.validation.ValidationLevel;
import basics.validation.ValidationReport;
import java.util.Objects;
import org.neo4j.graphdb.Node;

public abstract class PropertyValuePair<T> {

  private PropertyDescriptor<T> _property;

  private T _value;

  protected PropertyValuePair(PropertyDescriptor<T> property, T value) {
    if (property == null) {
      throw new IllegalArgumentException("property must not be null");
    }
    if (value == null) {
      throw new IllegalArgumentException("value must not be null");
    }
    _property = property;
    _value = value;
  }

  public String dbKey() {
    return _property.dbKey();
  }

  public String logKey() {
    return _property.logKey();
  }

  public PropertyDescriptor<T> getProperty() {
    return _property;
  }

  public T getValue() {
    return _value;
  }

  public boolean appliesTo(Node node) {
    if (!getProperty().appliesTo(node)) {
      return false;
    }
    T nodeValue = this.getProperty().getValueOn(node);
    return Objects.equals(nodeValue, this.getValue());
  }

  public String getValueCypher() {
    return ObjectValueCypherUtil.object2Cypher(getValue());
  }

  public boolean validate(Node node, ValidationReport report2Append) {
    if (!this.getProperty().validate(node, report2Append)) {
      return false;
    }
    T value = getProperty().getValueOn(node);
    if (Objects.deepEquals(value, getValue())) {
      String msg = String.format(
        "Unexpected value for %s. Value found: %s. Value expected: %s",
        dbKey(),
        ObjectValueCypherUtil.object2Cypher(value),
        ObjectValueCypherUtil.object2Cypher(getValue())
      );
      report2Append.append("Property Error", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }

  public abstract PropertyValuePair<T> getCopy();
}
