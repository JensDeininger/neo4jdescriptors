package basics.property;

public enum PropertyType {
  Any,
  Long,
  LongEnum, // <- that does not exist in neo4j. I use it to model enums backed by long fields
  Double,
  DoubleEnum, // <- that does not exist in neo4j. I use it to model enums backed by double fields
  Number, // <- that does not exist in neo4j. I use it to combine Float and Long
  String,
  StringEnum, // <- that does not exist in neo4j. I use it to model enums backed by string fields
  Boolean,
  // Point,
  // Date,
  // Time,
  // LocalTime,
  // DateTime,
  // LocalDateTime,
  // Duration;
}
