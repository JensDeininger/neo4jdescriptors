package basics.validation;

public enum ValidationLevel {
  DEBUG(1),
  INFO(2),
  WARNING(3),
  ERROR(4),
  FATAL(5);

  private int _val;

  ValidationLevel(int val) {
    this._val = val;
  }

  public int value() {
    return _val;
  }
}
