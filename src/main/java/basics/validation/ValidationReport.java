package basics.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class ValidationReport {

  private String _header = "";

  private TreeMap<String, ValidationReportSection> _data = new TreeMap<>();

  public ValidationReport(String header) {
    _header = header.equals(null) ? "" : header;
  }

  public void append(
    String sectionHeader,
    ValidationLevel level,
    String message
  ) {
    if (
      sectionHeader == null ||
      sectionHeader.length() == 0 ||
      message == null ||
      message.length() == 0
    ) {
      return;
    }
    ValidationReportSection section = _data.get(sectionHeader);
    if (section == null) {
      section = new ValidationReportSection(sectionHeader);
      _data.put(sectionHeader, section);
    }
    section.append(level, message);
  }

  public ValidationLevel highestMessageLevel() {
    ValidationLevel max = ValidationLevel.values()[0];
    for (ValidationReportSection sectionReport : _data.values()) {
      ValidationLevel sectionMax = sectionReport.highestMessageLevel();
      if (sectionMax.value() > max.value()) {
        max = sectionMax;
      }
    }
    return max;
  }

  public boolean containsMessages(Iterable<ValidationLevel> levels) {
    for (ValidationReportSection section : _data.values()) {
      if (section.containsMessages(levels)) {
        return true;
      }
    }
    return false;
  }

  public boolean containsMessages(ValidationLevel minLevel) {
    ValidationLevel[] foo = Arrays
      .stream(ValidationLevel.values())
      .filter(v -> v.value() >= minLevel.value())
      .toArray(ValidationLevel[]::new);
    return containsMessages(Arrays.asList(foo));
  }

  public boolean containsMessages(
    ValidationLevel level,
    ValidationLevel... moreLevels
  ) {
    ArrayList<ValidationLevel> levelList = new ArrayList<>();
    levelList.add(level);
    Collections.addAll(levelList, moreLevels);
    return containsMessages(levelList);
  }

  public Set<String> sectionHeaders() {
    return Collections.unmodifiableSet(_data.keySet());
  }

  public ValidationReportSection getSection(String sectionHeader) {
    return _data.get(sectionHeader);
  }

  /* Do NOT name these methods the same as the corresponding ones in NodeDescriptorValidationReport
   * This just screws things up what with that class both inheriting from and containing
   * instances of this class */

  public String printReport(
    String globalIndent,
    String subItemIndent,
    Iterable<ValidationLevel> levelList
  ) {
    StringBuilder strB = new StringBuilder(
      globalIndent + _header.replace(System.lineSeparator(), " ").trim()
    );
    for (String sectionHeader : _data.keySet()) {
      ValidationReportSection sectionReport = _data.get(sectionHeader);
      if (sectionReport != null) {
        String sectionReportString = sectionReport.printSection(
          globalIndent,
          subItemIndent,
          levelList
        );
        if (sectionReportString.length() > 0) {
          strB.append(System.lineSeparator() + sectionReportString);
        }
      }
    }
    return strB.toString();
  }

  /* Do NOT name these methods the same as the corresponding ones in NodeDescriptorValidationReport
   * This just screws things up what with that class both inheriting from and containing
   * instances of this class */

  public String printReport(
    String globalIndent,
    String subItemIndent,
    ValidationLevel minLevel
  ) {
    List<ValidationLevel> levelList = Arrays
      .stream(ValidationLevel.values())
      .filter(v -> v.value() >= minLevel.value())
      .collect(Collectors.toList());
    return printReport(globalIndent, subItemIndent, levelList);
  }

  /* Do NOT name these methods the same as the corresponding ones in NodeDescriptorValidationReport
   * This just screws things up what with that class both inheriting from and containing
   * instances of this class */

  public String printReport(
    String globalIndent,
    String subItemIndent,
    ValidationLevel level,
    ValidationLevel... moreLevels
  ) {
    ArrayList<ValidationLevel> levelList = new ArrayList<>();
    levelList.add(level);
    Collections.addAll(levelList, moreLevels);
    return printReport(globalIndent, subItemIndent, levelList);
  }

  public static void main(String[] args) {
    ValidationReport rep = new ValidationReport(
      "Dis be da header" +
      System.lineSeparator() +
      "and dis be some more header"
    );
    rep.append("Info", ValidationLevel.INFO, "dis be da info string");
    rep.append("Info", ValidationLevel.INFO, "dis be some more info string");
    rep.append("Something", ValidationLevel.WARNING, "keks");
    rep.append(
      "SomethingElse",
      ValidationLevel.WARNING,
      "keks" + System.lineSeparator() + "foos"
    );
    rep.append("Error", ValidationLevel.ERROR, "error_1");
    rep.append(
      "Error",
      ValidationLevel.ERROR,
      "error_2" + System.lineSeparator() + "some more text for error 2"
    );
    rep.append("Error", ValidationLevel.ERROR, "error_2");
    String s = rep.printReport("      ", "  ", ValidationLevel.INFO);
    System.out.println(s);
  }
}
