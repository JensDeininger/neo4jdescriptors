package basics.validation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.neo4j.graphdb.Node;

public class NodeDescriptorValidationReport extends ValidationReport {

  private HashMap<Node, ValidationReport> _node2reportMap = new HashMap<>();

  public NodeDescriptorValidationReport(String header) {
    super(header);
  }

  @Override
  public ValidationLevel highestMessageLevel() {
    ValidationLevel max = super.highestMessageLevel();
    for (ValidationReport nodeReport : _node2reportMap.values()) {
      ValidationLevel sectionMax = nodeReport.highestMessageLevel();
      if (sectionMax.value() > max.value()) {
        max = sectionMax;
      }
    }
    return max;
  }

  @Override
  public boolean containsMessages(ValidationLevel minLevel) {
    if (super.containsMessages(minLevel)) {
      return true;
    }
    for (ValidationReport rep : _node2reportMap.values()) {
      if (rep.containsMessages(minLevel)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean containsMessages(
    ValidationLevel level,
    ValidationLevel... moreLevels
  ) {
    if (super.containsMessages(level, moreLevels)) {
      return true;
    }
    for (ValidationReport rep : _node2reportMap.values()) {
      if (rep.containsMessages(level, moreLevels)) {
        return true;
      }
    }
    return false;
  }

  public int nodeReportCount(ValidationLevel minLevel) {
    int booCount = 0;
    for (ValidationReport rep : _node2reportMap.values()) {
      if (rep.containsMessages(minLevel)) {
        booCount++;
      }
    }
    return booCount;
  }

  public int nodeReportCount(
    ValidationLevel level,
    ValidationLevel... moreLevels
  ) {
    int booCount = 0;
    for (ValidationReport rep : _node2reportMap.values()) {
      if (rep.containsMessages(level, moreLevels)) {
        booCount++;
      }
    }
    return booCount;
  }

  public void append(Node node, ValidationReport report) {
    _node2reportMap.put(node, report);
  }

  public int nodeReportCount() {
    return _node2reportMap.size();
  }

  public Map<Node, ValidationReport> node2ReportMap() {
    return Collections.unmodifiableMap(_node2reportMap);
  }

  /* Do NOT name these methods the same as the corresponding ones in ValidationReport
   * This just screws things up what with this class both inheriting from and containing
   * instances of that class  */

  public String printDescriptorReport(
    String globalIndent,
    String subItemIndent,
    Iterable<ValidationLevel> levelList
  ) {
    StringBuilder strB = new StringBuilder(
      super.printReport(globalIndent, subItemIndent, levelList)
    );
    for (Node node : _node2reportMap.keySet()) {
      ValidationReport nodeReport = _node2reportMap.get(node);
      if (nodeReport.containsMessages(levelList)) {
        strB.append(System.lineSeparator());
        strB.append(
          nodeReport.printReport(subItemIndent, subItemIndent, levelList)
        );
      }
    }
    return strB.toString();
  }

  /* Do NOT name these methods the same as the corresponding ones in ValidationReport
   * This just screws things up what with this class both inheriting from and containing
   * instances of that class  */

  public String printDescriptorReport(
    String globalIndent,
    String subItemIndent,
    ValidationLevel minLevel
  ) {
    StringBuilder strB = new StringBuilder(
      super.printReport(globalIndent, subItemIndent, minLevel)
    );
    for (Node node : _node2reportMap.keySet()) {
      ValidationReport nodeReport = _node2reportMap.get(node);
      if (nodeReport.containsMessages(minLevel)) {
        strB.append(System.lineSeparator());
        strB.append(
          nodeReport.printReport(subItemIndent, subItemIndent, minLevel)
        );
      }
    }
    return strB.toString();
  }

  /* Do NOT name these methods the same as the corresponding ones in ValidationReport
   * This just screws things up what with this class both inheriting from and containing
   * instances of that class  */

  public String printDescriptorReport(
    String globalIndent,
    String subItemIndent,
    ValidationLevel level,
    ValidationLevel... moreLevels
  ) {
    StringBuilder strB = new StringBuilder(
      super.printReport(globalIndent, subItemIndent, level, moreLevels)
    );
    for (Node node : _node2reportMap.keySet()) {
      ValidationReport nodeReport = _node2reportMap.get(node);
      if (nodeReport.containsMessages(level, moreLevels)) {
        strB.append(System.lineSeparator());
        strB.append(
          nodeReport.printReport(
            subItemIndent,
            subItemIndent,
            level,
            moreLevels
          )
        );
      }
    }
    return strB.toString();
  }
}
