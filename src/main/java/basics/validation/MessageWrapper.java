package basics.validation;

public class MessageWrapper {

  public final String Msg;

  public MessageWrapper(String error) {
    this.Msg = error;
  }

  @Override
  public String toString() {
    return Msg;
  }
}
