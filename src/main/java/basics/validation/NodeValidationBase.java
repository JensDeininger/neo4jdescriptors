package basics.validation;

import basics.NodeDescriptor;
import basics.repository.NodeDescriptorRepository;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.kernel.impl.core.NodeEntity;

public class NodeValidationBase {

  private NodeDescriptorRepository _nodeRepository;

  public NodeValidationBase(NodeDescriptorRepository nodeRepository) {
    _nodeRepository = nodeRepository;
  }

  public Stream<MessageWrapper> verifyFindNodesIsMatch(Transaction tx) {
    ArrayList<MessageWrapper> failures = new ArrayList<>();
    for (NodeDescriptor nodeDescriptor : _nodeRepository
      .nodeDescriptors()
      .toArray(NodeDescriptor[]::new)) {
      try {
        Set<Node> nodes = nodeDescriptor
          .findNodes(tx)
          .collect(Collectors.toSet());
        for (Node node : nodes) {
          if (!nodeDescriptor.isMatch(node)) {
            String msg = String.format(
              "%s.isMatch(node) failed on Node<%d> ( MATCH (n) WHERE ID(n) = %d RETURN n )",
              nodeDescriptor.getClass().getName(),
              node.getId(),
              node.getId()
            );
            failures.add(new MessageWrapper(msg));
          }
        }
      } catch (Exception e) {
        System.out.println(
          "verifyFindNodesIsMatch failed for " +
          nodeDescriptor.getClass().getName()
        );
        e.printStackTrace();
      }
    }
    return failures.stream();
  }

  public Stream<MessageWrapper> verifyCypherFindNodes(Transaction tx) {
    ArrayList<MessageWrapper> failures = new ArrayList<>();
    for (NodeDescriptor nodeDescriptor : _nodeRepository
      .nodeDescriptors()
      .toArray(NodeDescriptor[]::new)) {
      try {
        HashSet<Long> cypherIds = new HashSet<>();
        String cypher = nodeDescriptor.getNodeIdentifier().getCypherMatch("n");
        try (Result result = tx.execute(cypher)) {
          while (result.hasNext()) {
            Map<String, Object> row = result.next();
            Object nodeObj = row.get("n");
            NodeEntity cast = (NodeEntity) nodeObj;
            long id = cast.getId();
            cypherIds.add(id);
          }
        }
        Set<Long> nodeIdsByFind = nodeDescriptor
          .findNodes(tx)
          .map(n -> n.getId())
          .collect(Collectors.toSet());
        boolean cypherIdsContainFindIds = cypherIds.containsAll(nodeIdsByFind);
        boolean findIdsContainCypherIds = nodeIdsByFind.containsAll(cypherIds);
        if (!cypherIdsContainFindIds || !findIdsContainCypherIds) {
          failures.add(
            new MessageWrapper(
              "verifyCypherFindNodes failed for " +
              nodeDescriptor.getClass().getName()
            )
          );
        }
      } catch (Exception e) {
        System.out.println(
          "cypherFindNodesTest failed for " + nodeDescriptor.toString()
        );
        e.printStackTrace();
        System.out.println(e);
      }
    }
    return failures.stream();
  }

  public Stream<MessageWrapper> validateNodes(
    Transaction tx,
    String minLevelName
  ) {
    ValidationLevel minLevel = ValidationLevel.valueOf(minLevelName);
    ArrayList<MessageWrapper> reports = new ArrayList<>();
    for (NodeDescriptor nodeDescriptor : _nodeRepository
      .nodeDescriptors()
      .toArray(NodeDescriptor[]::new)) {
      try {
        NodeDescriptorValidationReport descriptorReport = nodeDescriptor.validate(
          tx
        );
        if (descriptorReport.containsMessages(minLevel)) {
          reports.add(
            new MessageWrapper(
              System.lineSeparator() +
              descriptorReport.printDescriptorReport("", "  ", minLevel)
            )
          );
        }
      } catch (Exception e) {
        System.out.println(
          "validateNodesTest failed for " + nodeDescriptor.toString()
        );
        e.printStackTrace();
        System.out.println(e);
      }
    }
    return reports.stream();
  }
}
