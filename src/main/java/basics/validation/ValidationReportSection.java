package basics.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class ValidationReportSection {

  private String _header;

  private HashMap<ValidationLevel, ArrayList<String>> _data = new HashMap<>();

  public ValidationReportSection(String header) {
    if (header == null || header.length() == 0) {
      throw new IllegalArgumentException("header must not be null");
    }
    _header = header;
  }

  public String header() {
    return _header;
  }

  public void append(ValidationLevel level, String message) {
    if (level == null || message == null || message.length() == 0) {
      return;
    }
    ArrayList<String> levelMessages = _data.get(level);
    if (levelMessages == null) {
      levelMessages = new ArrayList<>();
      _data.put(level, levelMessages);
    }
    levelMessages.add(message);
  }

  public ValidationLevel highestMessageLevel() {
    Set<ValidationLevel> keySet = _data.keySet();
    ValidationLevel max = ValidationLevel.values()[0];
    for (ValidationLevel validationLevel : keySet) {
      if (validationLevel.value() > max.value()) {
        max = validationLevel;
      }
    }
    return max;
  }

  public boolean containsMessages(Iterable<ValidationLevel> levels) {
    for (ValidationLevel level : levels) {
      ArrayList<String> sectionHeader2MessagesMap = _data.get(level);
      if (
        sectionHeader2MessagesMap != null &&
        sectionHeader2MessagesMap.size() > 0
      ) {
        return true;
      }
    }
    return false;
  }

  public boolean containsMessages(ValidationLevel minLevel) {
    ValidationLevel[] foo = Arrays
      .stream(ValidationLevel.values())
      .filter(v -> v.value() >= minLevel.value())
      .toArray(ValidationLevel[]::new);
    return containsMessages(Arrays.asList(foo));
  }

  public boolean containsMessages(
    ValidationLevel level,
    ValidationLevel... moreLevels
  ) {
    ArrayList<ValidationLevel> levelList = new ArrayList<>();
    levelList.add(level);
    Collections.addAll(levelList, moreLevels);
    return containsMessages(levelList);
  }

  public List<String> getMessages(Iterable<ValidationLevel> levels) {
    ArrayList<String> result = new ArrayList<>();
    if (levels != null) {
      for (ValidationLevel validationLevel : levels) {
        ArrayList<String> messages = _data.get(validationLevel);
        if (messages != null) {
          result.addAll(messages);
        }
      }
    }
    return result;
  }

  public List<String> getMessages(ValidationLevel minLevel) {
    ValidationLevel[] foo = Arrays
      .stream(ValidationLevel.values())
      .filter(v -> v.value() >= minLevel.value())
      .toArray(ValidationLevel[]::new);
    return getMessages(Arrays.asList(foo));
  }

  public List<String> getMessages(
    ValidationLevel level,
    ValidationLevel... moreLevels
  ) {
    ArrayList<ValidationLevel> levelList = new ArrayList<>();
    levelList.add(level);
    Collections.addAll(levelList, moreLevels);
    return getMessages(levelList);
  }

  public String printSection(
    String globalIndent,
    String subIndent,
    Iterable<ValidationLevel> levels
  ) {
    List<String> messages = getMessages(levels);
    if (messages == null || messages.size() == 0) {
      return "";
    }
    StringBuilder strB = new StringBuilder(
      globalIndent + _header.replace(System.lineSeparator(), " ").trim() + ":"
    );
    if (messages.size() == 1) {
      if (!messages.get(0).contains(System.lineSeparator())) {
        strB.append(" " + messages.get(0));
      } else {
        String[] splits = messages.get(0).split(System.lineSeparator());
        String messagePart = String.join(
          System.lineSeparator() + globalIndent + subIndent,
          splits
        );
        strB.append(
          System.lineSeparator() + globalIndent + subIndent + messagePart
        );
      }
    } else {
      int itemCount = 1;
      for (String item : messages) {
        strB.append(System.lineSeparator());
        String itemPrefix = globalIndent + subIndent + itemCount++ + ": ";
        String subItemIndent = new String(new char[itemPrefix.length()])
        .replace('\0', ' ');
        String[] splits = item.split(System.lineSeparator());
        strB.append(
          itemPrefix +
          String.join(System.lineSeparator() + subItemIndent, splits)
        );
      }
    }
    return strB.toString();
  }

  public String printSection(
    String globalIndent,
    String subIndent,
    ValidationLevel minLevel
  ) {
    ValidationLevel[] foo = Arrays
      .stream(ValidationLevel.values())
      .filter(v -> v.value() >= minLevel.value())
      .toArray(ValidationLevel[]::new);
    return printSection(globalIndent, subIndent, Arrays.asList(foo));
  }

  public String printSection(
    String globalIndent,
    String subIndent,
    ValidationLevel level,
    ValidationLevel... moreLevels
  ) {
    ArrayList<ValidationLevel> levelList = new ArrayList<>();
    levelList.add(level);
    Collections.addAll(levelList, moreLevels);
    return printSection(globalIndent, subIndent, levelList);
  }
}
