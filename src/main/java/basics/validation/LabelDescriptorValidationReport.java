package basics.validation;

import basics.LabelDescriptor;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.neo4j.graphdb.Label;

public class LabelDescriptorValidationReport {

  private LabelDescriptor _labelDescriptor;

  private HashMap<Label, ValidationReport> _label2reportMap = new HashMap<>();

  public LabelDescriptorValidationReport(LabelDescriptor labelDescriptor) {
    _labelDescriptor = labelDescriptor;
  }

  public LabelDescriptor labelDescriptor() {
    return _labelDescriptor;
  }

  public void append(Label label, ValidationReport report) {
    _label2reportMap.put(label, report);
  }

  public int labelReportCount() {
    return _label2reportMap.size();
  }

  public Map<Label, ValidationReport> label2ReportMap() {
    return Collections.unmodifiableMap(_label2reportMap);
  }
}
