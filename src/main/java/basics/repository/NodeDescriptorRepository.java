package basics.repository;

import basics.NodeDescriptor;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class NodeDescriptorRepository {

  // region singleton stuff

  private static NodeDescriptorRepository INSTANCE;

  public static NodeDescriptorRepository getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new NodeDescriptorRepository();
    }
    return INSTANCE;
  }

  //endregion

  private HashSet<NodeDescriptor> _nodeDescriptors;

  /**
   * Goes up the inheritance chain of the specified startClass and collects it as well as
   * all super classes until it reaches null.
   */
  private Set<Class<?>> getInheritanceChain(Class<?> startClass) {
    HashSet<Class<?>> result = new HashSet<>();
    Class<?> c = startClass;
    do {
      result.add(c);
      c = c.getSuperclass();
    } while (c != null);
    return result;
  }

  private HashSet<NodeDescriptor> initNodeDescriptors() {
    Field[] allFields = this.getClass().getFields();
    HashSet<NodeDescriptor> result = new HashSet<>();
    for (Field field : allFields) {
      Class<?> fieldType = field.getType();
      Set<Class<?>> superClasses = getInheritanceChain(fieldType);
      if (superClasses.contains(NodeDescriptor.class)) {
        NodeDescriptor nodeDesc;
        try {
          nodeDesc = (NodeDescriptor) field.get(this);
        } catch (IllegalArgumentException | IllegalAccessException e) {
          throw new IllegalStateException(
            "Unexpected exception in NodeDescriptorRepository"
          );
        }
        result.add(nodeDesc);
      }
    }
    return result;
  }

  protected NodeDescriptorRepository() {}

  public Stream<NodeDescriptor> nodeDescriptors() {
    if (_nodeDescriptors == null) {
      _nodeDescriptors = initNodeDescriptors();
    }
    return _nodeDescriptors.stream();
  }
}
