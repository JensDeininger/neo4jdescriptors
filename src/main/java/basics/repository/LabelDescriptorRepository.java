package basics.repository;

import basics.LabelDescriptor;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.stream.Stream;

/**
 * A {@link LabelDescriptorRepository} is a singleton that is supposed to be extended
 * by a database specific class that declares all {@link LabelDescriptor} as public fields.
 * <p>
 * It provides the functionality of collecting all those via reflection and making them
 * accessible in the {@link #labelDescriptors() labelDescriptors()} method.
 */
public class LabelDescriptorRepository {

  // region singleton stuff

  private static LabelDescriptorRepository INSTANCE;

  public static LabelDescriptorRepository getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new LabelDescriptorRepository();
    }
    return INSTANCE;
  }

  //endregion

  private HashSet<LabelDescriptor> _labelDescriptors;

  private HashSet<LabelDescriptor> initLabelDescriptors() {
    Field[] allFields = this.getClass().getFields();
    HashSet<LabelDescriptor> result = new HashSet<>();
    for (Field field : allFields) {
      try {
        if (field.getClass().equals(LabelDescriptor.class)) {
          LabelDescriptor lblDesc = (LabelDescriptor) field.get(this);
          result.add(lblDesc);
        }
      } catch (IllegalArgumentException | IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }
    return result;
  }

  protected LabelDescriptorRepository() {}

  public Stream<LabelDescriptor> labelDescriptors() {
    if (_labelDescriptors == null) {
      _labelDescriptors = initLabelDescriptors();
    }
    return _labelDescriptors.stream();
  }
}
