package basics.repository;

import basics.relationship.*;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.stream.Stream;

/**
 * A {@link RelationshipTypeDescriptorRepository} is a singleton that is supposed to be extended
 * by a database specific class that declares all {@link RelationshipTypeDescriptor} as public fields.
 * <p>
 * It provides the functionality of collecting all those via reflection and making them
 * accessible in the {@link #relationshipTypeDescriptors() relationshipTypeDescriptors()} method.
 */
public class RelationshipTypeDescriptorRepository {

  // region singleton stuff

  private static RelationshipTypeDescriptorRepository INSTANCE;

  public static RelationshipTypeDescriptorRepository getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new RelationshipTypeDescriptorRepository();
    }
    return INSTANCE;
  }

  //endregion

  private HashSet<RelationshipTypeDescriptor> _relationshipTypeDescriptors;

  private HashSet<RelationshipTypeDescriptor> initRelationshipTypeDescriptors() {
    Field[] allFields = this.getClass().getFields();
    HashSet<RelationshipTypeDescriptor> result = new HashSet<>();
    for (Field field : allFields) {
      try {
        if (field.getClass().equals(RelationshipTypeDescriptor.class)) {
          RelationshipTypeDescriptor relDesc = (RelationshipTypeDescriptor) field.get(
            this
          );
          result.add(relDesc);
        }
      } catch (IllegalArgumentException | IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }
    return result;
  }

  protected RelationshipTypeDescriptorRepository() {}

  public Stream<RelationshipTypeDescriptor> relationshipTypeDescriptors() {
    if (_relationshipTypeDescriptors == null) {
      _relationshipTypeDescriptors = initRelationshipTypeDescriptors();
    }
    return _relationshipTypeDescriptors.stream();
  }
}
