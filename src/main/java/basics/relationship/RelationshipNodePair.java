package basics.relationship;

import basics.NodeDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.apache.commons.lang3.NotImplementedException;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;

/**
 * A {@link RelationshipNodePair} describes a directed relationship to a node matching a certain
 * {@link NodeDescriptor}.
 * <p>
 * {@link RelationshipNodePair} overrides {@link #equals() equals()} and
 * {@link #hashCode() hashCode()} so that identity is determined only by its values
 * (whose identity in turn is also determined by their values).
 * <p>
 * A {@link RelationshipNodePair} and its values are immutable, so it can safely be
 * used in {@link HashSet}, {@link HashMap} and the like.
 */
public class RelationshipNodePair<T extends NodeDescriptor> {

  private final Class<T> _typeParameterClass;

  private int _hash;

  private RelationshipDescriptor _relationDesc;

  private T _otherNode;

  public RelationshipNodePair(
    RelationshipDescriptor relationDesc,
    Class<T> typeParameterClass
  ) {
    if (relationDesc == null) {
      throw new IllegalArgumentException("relationDesc must not be null");
    }
    if (typeParameterClass == null) {
      throw new IllegalArgumentException("typeParameterClass must not be null");
    }
    // I can NOT call getInstance on that T, i.e. I can NOT initialise _otherNode. Because that
    // might have a Relationship to the class whose ctor called this here
    _typeParameterClass = typeParameterClass;
    _relationDesc = relationDesc;
    _hash = _relationDesc.hashCode();
    _hash = (_hash * 397) ^ _typeParameterClass.hashCode();
  }

  public RelationshipDescriptor relationshipDescriptor() {
    return _relationDesc;
  }

  public RelationshipType relationshipType() {
    return _relationDesc.relationshipType();
  }

  public Direction direction() {
    return _relationDesc.direction();
  }

  public T otherNodeDescriptor() {
    if (_otherNode == null) {
      try {
        Method m = _typeParameterClass.getMethod("getInstance");
        Object o = m.invoke(null);
        _otherNode = (T) o;
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
    return _otherNode;
  }

  public Stream<Node> otherNodes(Node node) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    if (!node.hasRelationship(this.direction(), this.relationshipType())) {
      String dirStr;
      switch (this.direction()) {
        case INCOMING:
          dirStr = "an incoming";
          break;
        case OUTGOING:
          dirStr = "an outgoing";
          break;
        case BOTH:
          dirStr = "a";
          break;
        default:
          throw new NotImplementedException();
      }
      throw new IllegalArgumentException(
        String.format(
          "node does not have %s %s relationship",
          dirStr,
          this.relationshipType().name()
        )
      );
    }
    Iterable<Relationship> relationships = node.getRelationships(
      this.direction(),
      this.relationshipType()
    );
    return StreamSupport
      .stream(relationships.spliterator(), false)
      .map(o -> o.getOtherNode(node));
  }

  public RelationshipNodePair<T> getCopy() {
    return new RelationshipNodePair<T>(
      _relationDesc.getCopy(),
      _typeParameterClass
    );
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    RelationshipNodePair<?> cast = (RelationshipNodePair<?>) that;
    if (!this._typeParameterClass.equals(cast._typeParameterClass)) {
      return false;
    }
    if (!_relationDesc.equals(cast._relationDesc)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
