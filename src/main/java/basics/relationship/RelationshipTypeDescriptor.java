package basics.relationship;

import java.util.HashMap;
import java.util.HashSet;
import org.neo4j.graphdb.RelationshipType;

/**
 * A {@link RelationshipTypeDescriptor} describes a {@link RelationshipType} which can be retrived by the
 * {@link #relationshipType() relationshipType()} method.
 * <p>
 * It is fully defined by two {@link String}s, the dbName and the logName, both of
 * which must be specified in the constructor.
 * <p>
 * The dbName must be the name of the RelationshipType in the database.
 * <p>
 * The logName is optional and will be the same as the dbName if not specified in
 * the constructor. It can be used to record database operations in logs or archives
 * so that old records are not affected by changes to the RelationshipType name in the database.
 * <p>
 * {@link RelationshipTypeDescriptor} overrides {@link #equals() equals()} and
 * {@link #hashCode() hashCode()} so that identity is determined only by the name(s).
 * <p>
 * A {@link RelationshipTypeDescriptor} is immutable, so it can safely be used in {@link HashSet},
 * {@link HashMap} and the like.
 */
public class RelationshipTypeDescriptor {

  private int _hash;

  private RelationshipType _relType;

  private String _logName;

  public RelationshipTypeDescriptor(String dbName) {
    this(dbName, dbName);
  }

  public RelationshipTypeDescriptor(String dbName, String logName) {
    if (dbName == null || dbName.length() == 0) {
      throw new IllegalArgumentException("dbName can not be null or empty");
    }
    if (logName == null || logName.length() == 0) {
      throw new IllegalArgumentException("logName can not be null or empty");
    }
    _relType = RelationshipType.withName(dbName);
    _logName = logName;
    _hash = _relType.name().hashCode();
    _hash = (logName == null) ? _hash : (_hash * 397) ^ logName.hashCode();
  }

  public RelationshipType relationshipType() {
    return _relType;
  }

  public String dbName() {
    return _relType.name();
  }

  public String logName() {
    return _logName;
  }

  public RelationshipTypeDescriptor getCopy() {
    return new RelationshipTypeDescriptor(this._relType.name(), this._logName);
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    RelationshipTypeDescriptor cast = (RelationshipTypeDescriptor) that;
    if (!_relType.name().equals(cast._relType.name())) {
      return false;
    }
    if (!_logName.equals(cast._logName)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
