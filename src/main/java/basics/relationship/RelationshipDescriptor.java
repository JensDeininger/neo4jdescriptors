package basics.relationship;

import java.util.HashMap;
import java.util.HashSet;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.RelationshipType;

/**
 * A {@link RelationshipDescriptor} describes a directed relation and is thus defined by
 * a {@link RelationshipTypeDescriptor} and a {@link Direction}.
 * <p>
 * {@link RelationshipDescriptor} overrides {@link #equals() equals()} and
 * {@link #hashCode() hashCode()} so that identity is determined only by its values
 * (whose identity in turn is also determined by their values).
 * <p>
 * A {@link RelationshipTypeDescriptor} and its values are immutable, so it can safely be
 * used in {@link HashSet}, {@link HashMap} and the like.
 */
public class RelationshipDescriptor {

  private int _hash;

  private RelationshipTypeDescriptor _relType;

  private Direction _direction; // the direction of the relationship from the point of view of the owning NodeDescriptor

  /**
   * Creates a {@link RelationshipDescriptor} with the specified {@link RelationshipTypeDescriptor}
   * and {@link Direction.BOTH}, which is to be understood as describing either direction.
   * @param relType the type of this {@link RelationshipDescriptor}
   */
  public RelationshipDescriptor(RelationshipTypeDescriptor relType) {
    this(relType, Direction.BOTH);
  }

  /**
   * Creates a {@link RelationshipDescriptor} with the specified {@link RelationshipTypeDescriptor}
   * and {@link Direction}.
   * @param relType the type of this {@link RelationshipDescriptor}
   * @param direction the direction of this {@link RelationshipDescriptor}
   */
  public RelationshipDescriptor(
    RelationshipTypeDescriptor relType,
    Direction direction
  ) {
    if (relType == null) {
      throw new IllegalArgumentException("relType must not be null");
    }
    if (direction == null) {
      throw new IllegalArgumentException("direction must not be null");
    }
    _relType = relType;
    _direction = direction;
    _hash = relType.hashCode();
    _hash = (_hash * 397) ^ _direction.hashCode();
  }

  public RelationshipType relationshipType() {
    return _relType.relationshipType();
  }

  public RelationshipTypeDescriptor relationshipTypeDescriptor() {
    return _relType;
  }

  /**
   * Returns this {@link RelationshipDescriptor}s {@link Direction}. {@link Direction.BOTH} is to be understood as either
   * direction rather than both.
   * @return this {@link RelationshipDescriptor}s {@link Direction}
   */
  public Direction direction() {
    return _direction;
  }

  public RelationshipDescriptor getCopy() {
    return new RelationshipDescriptor(
      relationshipTypeDescriptor().getCopy(),
      direction()
    );
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    RelationshipDescriptor cast = (RelationshipDescriptor) that;
    if (!_relType.equals(cast._relType)) {
      return false;
    }
    if (!_direction.equals(cast._direction)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
