package basics;

import basics.property.*;
import basics.relationship.*;
import basics.validation.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.apache.commons.lang3.NotImplementedException;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;

/**
 * A {@link NodeDescriptor} serves to describe a set of similar {@link Node}s. This set is
 * defined by features that are specified by a {@link NodeIdentifier}.
 * A {@link NodeIdentifier} is basically an object that specifies Labels, Relationships and
 * Properties that the Nodes must or must not have.
 * <p>
 * A {@link NodeDescriptor} can further include public fields for Labels, Relationships and
 * Properties that the described {@link Node}s are supposed to posses. The existence of
 * these can be confirmed via a validation procedure. This is supposed to guard against changes
 * in the database that might break existing user defined procedures.
 * <p>
 * All database specific {@link NodeDescriptor}s must inherit from this base class and should
 * also be singletons.
 * <p>
 * Any public fields of the types {@link LabelDescriptor}, {@link RelationshipDescriptor},
 * {@link RelationshipTypeDescriptor}, {@link PropertyDescriptor} and {@link PropertyValuePairBase}
 * (and its derivations) are surveyed by the methods in this base class via reflection to be
 * accessible as collections and properly included in the validation.
 */
public class NodeDescriptor {

  // region singleton stuff

  private static NodeDescriptor INSTANCE;

  public static NodeDescriptor getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new NodeDescriptor();
    }
    return INSTANCE;
  }

  //endregion

  protected NodeIdentifier _nodeIdentifier;

  protected List<RelationshipDescriptor> _contingentRelationships;

  protected List<RelationshipNodePair<?>> _contingentRelationshipNodePairs;

  protected List<PropertyDescriptor<?>> _contingentProperties;

  protected List<PropertyValuePair<?>> _contingentPropertyValuePairs;

  protected NodeDescriptor() {
    _nodeIdentifier = NodeIdentifier.Empty(); // I can not close this because it has no required Label
  }

  public NodeIdentifier getNodeIdentifier() {
    return _nodeIdentifier;
  }

  /**
   * Goes up the inheritance chain of the specified startClass and collects it as well as
   * all super classes until it reaches null.
   */
  private Set<Class<?>> getInheritanceChain(Class<?> startClass) {
    HashSet<Class<?>> result = new HashSet<>();
    Class<?> c = startClass;
    do {
      result.add(c);
      c = c.getSuperclass();
    } while (c != null);
    return result;
  }

  private List<RelationshipDescriptor> initContingentRelationships() {
    Field[] allFields = this.getClass().getFields();
    ArrayList<RelationshipDescriptor> res = new ArrayList<>();
    for (Field field : allFields) {
      Class<?> fieldType = field.getType();
      Set<Class<?>> superClasses = getInheritanceChain(fieldType);
      if (superClasses.contains(RelationshipDescriptor.class)) {
        RelationshipDescriptor relDesc;
        try {
          relDesc = (RelationshipDescriptor) field.get(this);
        } catch (IllegalArgumentException | IllegalAccessException e) {
          throw new IllegalStateException(
            "Unexpected exception in NodeDescriptor.initRelationships()",
            e
          );
        }
        res.add(relDesc);
      }
    }
    return Collections.unmodifiableList(res);
  }

  private List<RelationshipNodePair<?>> initContingentRelationshipNodePairs() {
    Field[] allFields = this.getClass().getFields();
    ArrayList<RelationshipNodePair<?>> res = new ArrayList<RelationshipNodePair<?>>();
    for (Field field : allFields) {
      Class<?> fieldType = field.getType();
      Set<Class<?>> superClasses = getInheritanceChain(fieldType);
      if (superClasses.contains(RelationshipNodePair.class)) {
        RelationshipNodePair<?> relNodePair;
        try {
          relNodePair = (RelationshipNodePair<?>) field.get(this);
        } catch (IllegalArgumentException | IllegalAccessException e) {
          throw new IllegalStateException(
            "Unexpected exception in NodeDescriptor.initRelationships()"
          );
        }
        res.add(relNodePair);
      }
    }
    return Collections.unmodifiableList(res);
  }

  private List<PropertyDescriptor<?>> initContingentProperties() {
    Field[] allFields = this.getClass().getFields();
    ArrayList<PropertyDescriptor<?>> res = new ArrayList<PropertyDescriptor<?>>();
    for (Field field : allFields) {
      Class<?> fieldType = field.getType();
      Set<Class<?>> superClasses = getInheritanceChain(fieldType);
      if (superClasses.contains(PropertyDescriptor.class)) {
        PropertyDescriptor<?> propDesc;
        try {
          propDesc = (PropertyDescriptor<?>) field.get(this);
        } catch (IllegalArgumentException | IllegalAccessException e) {
          throw new IllegalStateException(
            "Unexpected exception in NodeDescriptor.initProperties()"
          );
        }
        res.add(propDesc);
      }
    }
    return Collections.unmodifiableList(res);
  }

  private List<PropertyValuePair<?>> initContingentPropertyValuePairs() {
    Field[] allFields = this.getClass().getFields();
    ArrayList<PropertyValuePair<?>> res = new ArrayList<>();
    for (Field field : allFields) {
      Class<?> fieldType = field.getType();
      Set<Class<?>> superClasses = getInheritanceChain(fieldType);
      if (superClasses.contains(PropertyValuePair.class)) {
        PropertyValuePair<?> propValuePair;
        try {
          propValuePair = (PropertyValuePair<?>) field.get(this);
        } catch (IllegalArgumentException | IllegalAccessException e) {
          throw new IllegalStateException(
            "Unexpected exception in NodeDescriptor.initProperties()"
          );
        }
        res.add(propValuePair);
      }
    }
    return Collections.unmodifiableList(res);
  }

  public Set<RelationshipDescriptor> getRelationships(Modality modality) {
    Set<RelationshipDescriptor> requiredRelationships = _nodeIdentifier.getRequiredRelationships();
    if (_contingentRelationships == null) {
      _contingentRelationships = initContingentRelationships();
    }
    HashSet<RelationshipDescriptor> result = new HashSet<>();
    switch (modality) {
      case NECESSARY:
        result.addAll(requiredRelationships);
        break;
      case CONTINGENT:
        result.addAll(_contingentRelationships);
        result.removeAll(requiredRelationships);
        break;
      case BOTH:
        result.addAll(_contingentRelationships);
        result.addAll(requiredRelationships);
        break;
      default:
        throw new NotImplementedException();
    }
    return result;
  }

  public Set<RelationshipNodePair<?>> getRelationshipNodePairs() {
    if (_contingentRelationshipNodePairs == null) {
      _contingentRelationshipNodePairs = initContingentRelationshipNodePairs();
    }
    HashSet<RelationshipNodePair<?>> result = new HashSet<>();
    result.addAll(_contingentRelationshipNodePairs);
    return result;
  }

  public Set<PropertyDescriptor<?>> getProperties(Modality modality) {
    Set<PropertyDescriptor<?>> requiredProperties = _nodeIdentifier.getRequiredProperties();
    if (_contingentProperties == null) {
      _contingentProperties = initContingentProperties();
    }
    HashSet<PropertyDescriptor<?>> result = new HashSet<>();
    switch (modality) {
      case NECESSARY:
        result.addAll(requiredProperties);
        break;
      case CONTINGENT:
        result.addAll(_contingentProperties);
        result.removeAll(requiredProperties);
        break;
      case BOTH:
        result.addAll(_contingentProperties);
        result.addAll(requiredProperties);
        break;
      default:
        throw new NotImplementedException();
    }
    return result;
  }

  public Set<PropertyValuePair<?>> getPropertyValuePairs(Modality modality) {
    Set<PropertyValuePair<?>> requiredPropValuePairs = _nodeIdentifier.getRequiredPropValuePairs();
    if (_contingentPropertyValuePairs == null) {
      _contingentPropertyValuePairs = initContingentPropertyValuePairs();
    }
    HashSet<PropertyValuePair<?>> result = new HashSet<>();
    switch (modality) {
      case NECESSARY:
        result.addAll(requiredPropValuePairs);
        break;
      case CONTINGENT:
        result.addAll(_contingentPropertyValuePairs);
        result.removeAll(requiredPropValuePairs);
        break;
      case BOTH:
        result.addAll(_contingentPropertyValuePairs);
        result.addAll(requiredPropValuePairs);
        break;
      default:
        throw new NotImplementedException();
    }
    return result;
  }

  public Stream<Node> findNodes(Transaction tx) {
    NodeIdentifier sig = this.getNodeIdentifier();
    Iterator<LabelDescriptor> requiredLabelIter = sig
      .getRequiredLabels()
      .iterator();
    if (!requiredLabelIter.hasNext()) {
      throw new IllegalStateException(
        "Every NodeIdentifier must have at least one required Label"
      );
    }
    LabelDescriptor firstLabel = requiredLabelIter.next();
    Stream<Node> nodeStream = tx.findNodes(firstLabel.label()).stream();
    while (requiredLabelIter.hasNext()) {
      LabelDescriptor lab = requiredLabelIter.next();
      nodeStream = nodeStream.filter(n -> n.hasLabel(lab.label()));
    }
    Iterator<LabelDescriptor> excludedLabelIter = sig
      .getExcludedLabels()
      .iterator();
    while (excludedLabelIter.hasNext()) {
      LabelDescriptor lab = excludedLabelIter.next();
      nodeStream = nodeStream.filter(n -> !n.hasLabel(lab.label()));
    }
    Iterator<RelationshipDescriptor> requiredRelDescIter = sig
      .getRequiredRelationships()
      .iterator();
    while (requiredRelDescIter.hasNext()) {
      RelationshipDescriptor relDesc = requiredRelDescIter.next();
      nodeStream =
        nodeStream.filter(
          n ->
            n.hasRelationship(relDesc.direction(), relDesc.relationshipType())
        );
    }
    Iterator<RelationshipDescriptor> excludedRelDescIter = sig
      .getExcludedRelationships()
      .iterator();
    while (excludedRelDescIter.hasNext()) {
      RelationshipDescriptor relDesc = excludedRelDescIter.next();
      nodeStream =
        nodeStream.filter(
          n ->
            !n.hasRelationship(relDesc.direction(), relDesc.relationshipType())
        );
    }
    Iterator<PropertyDescriptor<?>> propIter = sig
      .getRequiredProperties()
      .iterator();
    while (propIter.hasNext()) {
      PropertyDescriptor<?> prop = propIter.next();
      nodeStream = nodeStream.filter(prop::appliesTo);
    }
    Iterator<PropertyValuePair<?>> propValueIter = sig
      .getRequiredPropValuePairs()
      .iterator();
    while (propValueIter.hasNext()) {
      PropertyValuePair<?> pv = propValueIter.next();
      nodeStream = nodeStream.filter(pv::appliesTo);
    }
    return nodeStream;
  }

  private boolean validateContingentRelationship(
    Node node,
    RelationshipDescriptor relDesc,
    ValidationReport report2Addto
  ) {
    Iterable<Relationship> relationships = node.getRelationships(
      relDesc.direction(),
      relDesc.relationshipType()
    );
    long matchCount = StreamSupport
      .stream(relationships.spliterator(), false)
      .count();
    if (matchCount == 0) {
      String dirStr;
      switch (relDesc.direction()) {
        case INCOMING:
          dirStr = "an incoming";
          break;
        case OUTGOING:
          dirStr = "an outgoing";
          break;
        case BOTH:
          dirStr = "any";
          break;
        default:
          throw new NotImplementedException();
      }
      String msg = String.format(
        "Node is not connected by %s %s relation",
        dirStr,
        relDesc.relationshipType().name()
      );
      report2Addto.append("RelationshipError", ValidationLevel.ERROR, msg);
      return false;
    }

    if (
      !node.hasRelationship(relDesc.direction(), relDesc.relationshipType())
    ) {
      String msg = String.format(
        "found no relationship of type %s",
        relDesc.relationshipType().name()
      );
      report2Addto.append("Relationship Error", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }

  private boolean validateContingentRelationships(
    Node node,
    ValidationReport report2Addto
  ) {
    Iterator<RelationshipDescriptor> relationships =
      this.getRelationships(Modality.CONTINGENT).iterator();
    boolean yay = true;
    while (relationships.hasNext()) {
      RelationshipDescriptor relDesc = relationships.next();
      yay = validateContingentRelationship(node, relDesc, report2Addto) && yay;
    }
    return yay;
  }

  private boolean validateContingentRelationshipNodePair(
    Node node,
    RelationshipNodePair<?> relNodePair,
    ValidationReport report2Addto
  ) {
    Iterable<Relationship> relationships = node.getRelationships(
      relNodePair.direction(),
      relNodePair.relationshipType()
    );
    boolean anyMatches = StreamSupport
      .stream(relationships.spliterator(), false)
      .anyMatch(
        rel -> relNodePair.otherNodeDescriptor().isMatch(rel.getOtherNode(node))
      );
    if (!anyMatches) {
      String dirStr;
      switch (relNodePair.direction()) {
        case INCOMING:
          dirStr = "an incoming";
          break;
        case OUTGOING:
          dirStr = "an outgoing";
          break;
        case BOTH:
          dirStr = "any";
          break;
        default:
          throw new NotImplementedException();
      }
      String msg = String.format(
        "Node is not connected by %s %s relation to another node matching %s%s",
        dirStr,
        relNodePair.relationshipType().name(),
        System.lineSeparator(),
        relNodePair
          .otherNodeDescriptor()
          .getNodeIdentifier()
          .getCypherMatch("n")
      );
      report2Addto.append("RelationshipError", ValidationLevel.ERROR, msg);
      return false;
    }
    return true;
  }

  private boolean validateContingetRelationshipNodePairs(
    Node node,
    ValidationReport report2Addto
  ) {
    Set<RelationshipNodePair<?>> relationships =
      this.getRelationshipNodePairs();
    boolean yay = true;
    for (RelationshipNodePair<?> relDesc : relationships) {
      yay =
        validateContingentRelationshipNodePair(node, relDesc, report2Addto) &&
        yay;
    }
    return yay;
  }

  private boolean validateContingentProperty(
    Node node,
    PropertyDescriptor<?> propDesc,
    ValidationReport report2Addto
  ) {
    return propDesc.validate(node, report2Addto);
  }

  private boolean validateContingentProperties(
    Node node,
    ValidationReport report2Addto
  ) {
    Iterator<PropertyDescriptor<?>> propertyIter =
      this.getProperties(Modality.CONTINGENT).iterator();
    boolean yay = true;
    while (propertyIter.hasNext()) {
      PropertyDescriptor<?> pd = propertyIter.next();
      yay = validateContingentProperty(node, pd, report2Addto) && yay;
    }
    return yay;
  }

  private boolean validateContingentPropertyValuePair(
    Node node,
    PropertyValuePair<?> propValuePair,
    ValidationReport report2Addto
  ) {
    return propValuePair.validate(node, report2Addto);
  }

  private void validateContingentPropertyValuePairs(
    Node node,
    ValidationReport report2Addto
  ) {
    Iterator<PropertyValuePair<?>> propertyValuePairsIter =
      this.getPropertyValuePairs(Modality.CONTINGENT).iterator();
    while (propertyValuePairsIter.hasNext()) {
      PropertyValuePair<?> pvp = propertyValuePairsIter.next();
      validateContingentPropertyValuePair(node, pvp, report2Addto);
    }
  }

  protected void specificValidation(Node node, ValidationReport report2Add2) {
    // the derivations of this class can do specific stuff here that can not be expressed by declaring descriptors
  }

  public final NodeDescriptorValidationReport validate(Transaction tx) {
    NodeDescriptorValidationReport descriptorReport = new NodeDescriptorValidationReport(
      "ValidationReport: " + this.getClass().getName()
    );
    List<Node> nodes = findNodes(tx).collect(Collectors.toUnmodifiableList());
    if (nodes.isEmpty()) {
      descriptorReport.append(
        "Cypher",
        ValidationLevel.WARNING,
        this._nodeIdentifier.getCypherMatch("n")
      );
      descriptorReport.append(
        "Warning",
        ValidationLevel.WARNING,
        "No nodes found"
      );
      return descriptorReport;
    }
    ValidationLevel allNodesMax = ValidationLevel.values()[0];
    for (Node node : nodes) {
      ValidationReport nodeReport = new ValidationReport(
        String.format("Node MATCH (n) WHERE ID(n) = %d RETURN n", node.getId())
      );
      validateContingentRelationships(node, nodeReport);
      validateContingetRelationshipNodePairs(node, nodeReport);
      validateContingentProperties(node, nodeReport);
      validateContingentPropertyValuePairs(node, nodeReport);
      specificValidation(node, nodeReport);
      descriptorReport.append(node, nodeReport);
      ValidationLevel nodeMax = nodeReport.highestMessageLevel();
      if (nodeMax.value() > allNodesMax.value()) {
        allNodesMax = nodeMax;
      }
    }
    if (allNodesMax.value() < ValidationLevel.WARNING.value()) {
      descriptorReport.append(
        "Info",
        ValidationLevel.INFO,
        "found " +
        nodes.size() +
        " nodes, all of which were successfully validated"
      );
    } else {
      int failedNodeCount = descriptorReport.nodeReportCount(
        ValidationLevel.WARNING
      );
      String msg = String.format(
        "found %d nodes, of which %d failed to validate",
        nodes.size(),
        failedNodeCount
      );
      descriptorReport.append(allNodesMax.toString(), allNodesMax, msg);
    }
    ValidationLevel totalMax = descriptorReport.highestMessageLevel();
    if (totalMax.value() >= ValidationLevel.WARNING.value()) {
      descriptorReport.append(
        "Cypher",
        totalMax,
        this._nodeIdentifier.getCypherMatch("n")
      );
    }
    return descriptorReport;
  }

  public boolean isMatch(Node node) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    return _nodeIdentifier.isMatch(node);
  }
}
