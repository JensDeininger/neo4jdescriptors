package basics;

import java.util.HashMap;
import java.util.HashSet;
import org.neo4j.graphdb.Label;

/**
 * A {@link LabelDescriptor} describes to a {@link Label} which can be retrived by the
 * {@link #label() label()} method.
 * <p>
 * It is fully defined by two {@link String}s, the dbName and the logName, both of
 * which must be specified in the constructor.
 * <p>
 * The dbName must be the name of the Label in the database.
 * <p>
 * The logName is optional and will be the same as the dbName if not specified in
 * the constructor. It can be used to record database operations in logs or archives
 * so that old records are not affected by changes to the Label name in the database.
 * <p>
 * {@link LabelDescriptor} overrides {@link #equals() equals()} and
 * {@link #hashCode() hashCode()} so that identity is determined only by the name(s).
 * <p>
 * A {@link LabelDescriptor} is immutable, so it can safely be used in {@link HashSet},
 * {@link HashMap} and the like.
 */
public class LabelDescriptor {

  private Integer _hash = null;

  private Label _label;

  private String _logName;

  public LabelDescriptor(String dbName) {
    this(dbName, dbName);
  }

  public LabelDescriptor(String dbName, String logName) {
    if (dbName == null || dbName.length() == 0) {
      throw new IllegalArgumentException("dbName can not be null or empty");
    }
    if (logName == null || logName.length() == 0) {
      throw new IllegalArgumentException("logName can not be null or empty");
    }
    _label = Label.label(dbName);
    _logName = logName;
    _hash = _label.name().hashCode();
    _hash = (_hash * 397) ^ logName.hashCode();
  }

  public Label label() {
    return _label;
  }

  public String dbName() {
    return _label.name();
  }

  public String logName() {
    return _logName;
  }

  public LabelDescriptor getCopy() {
    return new LabelDescriptor(this._label.name(), this._logName);
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    LabelDescriptor cast = (LabelDescriptor) that;
    if (!_label.name().equals(cast._label.name())) {
      return false;
    }
    if (!_logName.equals(cast._logName)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    return _hash;
  }
}
