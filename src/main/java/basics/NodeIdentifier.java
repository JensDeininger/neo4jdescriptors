package basics;

import basics.property.*;
import basics.relationship.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;

/**
 * A {@link NodeIdentifier} serves to specify the set of {@link Node}s that a {@link NodeDescriptor}
 * describes.
 * <p>
 * It is possible to specify required and excluded {@link LabelDescriptor}s, {@link RelationshipDescriptor}s
 * and {@link PropertyDescriptor}s and {@link PropertyValuePairBase}s.
 * <p>
 * Creating a {@link NodeIdentifier} is best handled in a single fluent style command starting with calling the
 * static {@link #Empty() Empty()} method. It should always end with {@link #closeDefinition() closeDefinition()}.
 * This makes the {@link NodeIdentifier} immutable. A {@link NodeIdentifier} must have at least one required
 * {@link LabelDescriptor} before it can be closed.
 * <p>
 * {@link NodeIdentifier} overrides {@link #equals() equals()} and
 * {@link #hashCode() hashCode()} so that identity is determined only by its values
 * (whose identity in turn is also determined by their values).
 * <p>
 * Once closed by calling {@link #closeDefinition() closeDefinition()}, a {@link NodeIdentifier} is
 * immutable (as are all its values), so it can safely be used in {@link HashSet}, {@link HashMap} and the like.
 */
public class NodeIdentifier {

  // region Private Fields

  private Integer _hash;

  private boolean _isFinal = false;

  private HashSet<LabelDescriptor> _requiredLabels = new HashSet<LabelDescriptor>(); // the labels that the all corresponding nodes must have

  private HashSet<LabelDescriptor> _excludedLabels = new HashSet<LabelDescriptor>(); // the labels that the all corresponding nodes must NOT have

  private HashSet<RelationshipDescriptor> _requiredRelationships = new HashSet<>();

  private HashSet<RelationshipDescriptor> _excludedRelationships = new HashSet<>();

  /* There is an excellent reason why there must NOT be any RelationshipNodePairs in the NodeIdentifier:     

     Lets say there are three Nodes in a loop with the RelationshipNodePair between them as being part of their NodeIdentifier.
     If I then try to check if Node_1 matches the NodeDescriptor, I will have to check if Node_2 matches
     the specified NodeDescriptor, and thus on to Node_3 and back to Node_1.

     Bang! an infinite loop
   */

  private HashSet<PropertyDescriptor<?>> _requiredProperties = new HashSet<>();

  private HashSet<PropertyDescriptor<?>> _excludedProperties = new HashSet<>();

  private HashSet<PropertyValuePair<?>> _requiredPropValuePairs = new HashSet<>();

  // endregion

  private NodeIdentifier() {}

  public static NodeIdentifier Empty() {
    return new NodeIdentifier();
  }

  public Set<LabelDescriptor> getRequiredLabels() {
    return Collections.unmodifiableSet(_requiredLabels);
  }

  public Set<LabelDescriptor> getExcludedLabels() {
    return Collections.unmodifiableSet(_excludedLabels);
  }

  public Set<RelationshipDescriptor> getRequiredRelationships() {
    return Collections.unmodifiableSet(_requiredRelationships);
  }

  public Set<RelationshipDescriptor> getExcludedRelationships() {
    return Collections.unmodifiableSet(_excludedRelationships);
  }

  public Set<PropertyDescriptor<?>> getRequiredProperties() {
    return Collections.unmodifiableSet(_requiredProperties);
  }

  public Set<PropertyDescriptor<?>> getExcludedProperties() {
    return Collections.unmodifiableSet(_excludedProperties);
  }

  public Set<PropertyValuePair<?>> getRequiredPropValuePairs() {
    return Collections.unmodifiableSet(_requiredPropValuePairs);
  }

  public NodeIdentifier requires(LabelDescriptor labelDescriptor) {
    if (_isFinal) {
      throw new IllegalStateException(
        "Can not modify a finalized NodeIdentifier any further"
      );
    }
    if (labelDescriptor == null) {
      throw new IllegalArgumentException("label can not be null");
    }
    if (_excludedLabels.contains(labelDescriptor)) {
      throw new IllegalStateException(
        "label has already been declared as excluded"
      );
    }
    _requiredLabels.add(labelDescriptor);
    return this;
  }

  public NodeIdentifier excludes(LabelDescriptor labelDescriptor) {
    if (_isFinal) {
      throw new IllegalStateException(
        "Can not modify a finalized NodeIdentifier any further"
      );
    }
    if (labelDescriptor == null) {
      throw new IllegalArgumentException("label can not be null");
    }
    if (_requiredLabels.contains(labelDescriptor)) {
      throw new IllegalStateException(
        "label has already been declared as required"
      );
    }
    _excludedLabels.add(labelDescriptor);
    return this;
  }

  public NodeIdentifier requires(
    RelationshipDescriptor relationshipDescriptor
  ) {
    if (_isFinal) {
      throw new IllegalStateException(
        "Can not modify a finalized NodeIdentifier any further"
      );
    }
    if (relationshipDescriptor == null) {
      throw new IllegalArgumentException(
        "relationshipDescriptor can not be null"
      );
    }
    if (_excludedRelationships.contains(relationshipDescriptor)) {
      throw new IllegalStateException(
        "label has already been declared as excluded"
      );
    }
    _requiredRelationships.add(relationshipDescriptor);
    return this;
  }

  public NodeIdentifier excludes(
    RelationshipDescriptor relationshipDescriptor
  ) {
    if (_isFinal) {
      throw new IllegalStateException(
        "Can not modify a finalized NodeIdentifier any further"
      );
    }
    if (relationshipDescriptor == null) {
      throw new IllegalArgumentException(
        "relationshipDescriptor can not be null"
      );
    }
    if (_requiredRelationships.contains(relationshipDescriptor)) {
      throw new IllegalStateException(
        "label has already been declared as required"
      );
    }
    _excludedRelationships.add(relationshipDescriptor);
    return this;
  }

  public NodeIdentifier requires(PropertyDescriptor<?> propertyDescriptor) {
    if (_isFinal) {
      throw new IllegalStateException(
        "Can not modify a finalized NodeIdentifier any further"
      );
    }
    if (propertyDescriptor == null) {
      throw new IllegalArgumentException("prop can not be null");
    }
    if (_excludedProperties.contains(propertyDescriptor)) {
      throw new IllegalStateException(
        "label has already been declared as excluded"
      );
    }
    _requiredProperties.add(propertyDescriptor);
    return this;
  }

  public NodeIdentifier excludes(PropertyDescriptor<?> propertyDescriptor) {
    if (_isFinal) {
      throw new IllegalStateException(
        "Can not modify a finalized NodeIdentifier any further"
      );
    }
    if (propertyDescriptor == null) {
      throw new IllegalArgumentException("prop can not be null");
    }
    if (_requiredProperties.contains(propertyDescriptor)) {
      throw new IllegalStateException(
        "label has already been declared as required"
      );
    }
    _excludedProperties.add(propertyDescriptor);
    return this;
  }

  public NodeIdentifier requires(PropertyValuePair<?> propValuePair) {
    if (_isFinal) {
      throw new IllegalStateException(
        "Can not modify a finalized NodeIdentifier any further"
      );
    }
    if (propValuePair == null) {
      throw new IllegalArgumentException("propertyKey can not be null");
    }
    PropertyDescriptor<?> prop = propValuePair.getProperty();
    if (_requiredProperties.contains(prop)) {
      _requiredProperties.remove(prop);
    }
    _requiredPropValuePairs.add(propValuePair);
    return this;
  }

  public NodeIdentifier closeDefinition() {
    _isFinal = true;
    return this;
  }

  public NodeIdentifier getOpenCopy() {
    NodeIdentifier copy = new NodeIdentifier();
    copy._requiredLabels =
      _requiredLabels
        .stream()
        .map(l -> l.getCopy())
        .collect(Collectors.toCollection(HashSet::new));
    copy._excludedLabels =
      _excludedLabels
        .stream()
        .map(l -> l.getCopy())
        .collect(Collectors.toCollection(HashSet::new));
    copy._requiredRelationships =
      _requiredRelationships
        .stream()
        .map(l -> l.getCopy())
        .collect(Collectors.toCollection(HashSet::new));
    copy._excludedRelationships =
      _excludedRelationships
        .stream()
        .map(l -> l.getCopy())
        .collect(Collectors.toCollection(HashSet::new));
    copy._requiredProperties =
      _requiredProperties
        .stream()
        .map(p -> p.getCopy())
        .collect(Collectors.toCollection(HashSet::new));
    copy._excludedProperties =
      _excludedProperties
        .stream()
        .map(p -> p.getCopy())
        .collect(Collectors.toCollection(HashSet::new));
    copy._requiredPropValuePairs =
      _requiredPropValuePairs
        .stream()
        .map(p -> p.getCopy())
        .collect(Collectors.toCollection(HashSet::new));
    copy._hash = null;
    copy._isFinal = false;
    return copy;
  }

  public NodeIdentifier getClosedCopy() {
    NodeIdentifier copy = getOpenCopy().closeDefinition();
    return copy;
  }

  public String getCypherMatch(String nodeAlias) {
    String[] labelNames = _requiredLabels
      .stream()
      .map(l -> l.dbName())
      .toArray(String[]::new);
    String labelPart = String.format(
      "(%s:%s)",
      nodeAlias,
      String.join(":", labelNames)
    );
    if (labelNames.length == 0) {
      labelPart = String.format("(%s) ", nodeAlias);
    }

    String butNotLabelClause = "";
    if (_excludedLabels.size() > 0) {
      String[] butNotLabelClauses = _excludedLabels
        .stream()
        .map(l -> nodeAlias + ":" + l.dbName())
        .toArray(String[]::new);
      butNotLabelClause = "NOT " + String.join(" AND NOT ", butNotLabelClauses);
    }

    String requiredPropClause = "";
    if (_requiredProperties.size() > 0) {
      String[] propClauses = _requiredProperties
        .stream()
        .map(p -> String.format("EXISTS(%s.%s)", nodeAlias, p.dbKey()))
        .toArray(String[]::new);
      requiredPropClause = String.join(" AND ", propClauses);
    }

    String excludedPropClause = "";
    if (_excludedProperties.size() > 0) {
      String[] propClauses = _excludedProperties
        .stream()
        .map(p -> String.format("NOT EXISTS(%s.%s)", nodeAlias, p.dbKey()))
        .toArray(String[]::new);
      excludedPropClause = String.join(" AND ", propClauses);
    }

    String requiredRelationShipClause = "";
    if (_requiredRelationships.size() > 0) {
      Stream<String> outClauses = _requiredRelationships
        .stream()
        .filter(r -> r.direction() == Direction.OUTGOING)
        .map(
          r ->
            String.format(
              "(%s)-[:%s]->()",
              nodeAlias,
              r.relationshipTypeDescriptor().dbName()
            )
        );
      Stream<String> incClauses = _requiredRelationships
        .stream()
        .filter(r -> r.direction() == Direction.INCOMING)
        .map(
          r ->
            String.format(
              "(%s)<-[:%s]-()",
              nodeAlias,
              r.relationshipTypeDescriptor().dbName()
            )
        );
      Stream<String> bothClauses = _requiredRelationships
        .stream()
        .filter(r -> r.direction() == Direction.BOTH)
        .map(
          r ->
            String.format(
              "(%s)-[:%s]-()",
              nodeAlias,
              r.relationshipTypeDescriptor().dbName()
            )
        );
      String[] allClauses = Stream
        .concat(Stream.concat(outClauses, incClauses), bothClauses)
        .toArray(String[]::new);
      requiredRelationShipClause = String.join(" AND ", allClauses);
    }

    String excludedRelationShipClause = "";
    if (_excludedRelationships.size() > 0) {
      Stream<String> outClauses = _excludedRelationships
        .stream()
        .filter(r -> r.direction() == Direction.OUTGOING)
        .map(
          r ->
            String.format(
              "(%s)-[:%s]->()",
              nodeAlias,
              r.relationshipTypeDescriptor().dbName()
            )
        );
      Stream<String> incClauses = _excludedRelationships
        .stream()
        .filter(r -> r.direction() == Direction.INCOMING)
        .map(
          r ->
            String.format(
              "(%s)<-[:%s]-()",
              nodeAlias,
              r.relationshipTypeDescriptor().dbName()
            )
        );
      Stream<String> bothClauses = _excludedRelationships
        .stream()
        .filter(r -> r.direction() == Direction.BOTH)
        .map(
          r ->
            String.format(
              "(%s)-[:%s]-()",
              nodeAlias,
              r.relationshipTypeDescriptor().dbName()
            )
        );
      String[] allClauses = Stream
        .concat(Stream.concat(outClauses, incClauses), bothClauses)
        .toArray(String[]::new);
      excludedRelationShipClause =
        "NOT " + String.join(" AND NOT ", allClauses);
    }

    String propValueClause = "";
    if (_requiredPropValuePairs.size() > 0) {
      String[] propValueClauses = _requiredPropValuePairs
        .stream()
        .map(
          p ->
            String.format("%s.%s=%s", nodeAlias, p.dbKey(), p.getValueCypher())
        )
        .toArray(String[]::new);
      propValueClause = String.join(" AND ", propValueClauses);
    }

    String[] optionalClauses = Stream
      .of(
        butNotLabelClause,
        requiredRelationShipClause,
        excludedRelationShipClause,
        requiredPropClause,
        excludedPropClause,
        propValueClause
      )
      .filter(s -> s.length() > 0)
      .toArray(String[]::new);
    String optionalPart = String.join(" AND ", optionalClauses);
    if (optionalPart.length() != 0) {
      optionalPart =
        System.lineSeparator() +
        "WHERE " +
        optionalPart +
        System.lineSeparator();
    }
    String fullCypher = String.format(
      "MATCH %s%s RETURN %s",
      labelPart,
      optionalPart,
      nodeAlias
    );
    return fullCypher;
  }

  private boolean matchesLabels(Node node) {
    for (LabelDescriptor labelDescriptor : _requiredLabels) {
      if (!node.hasLabel(labelDescriptor.label())) {
        return false;
      }
    }
    for (LabelDescriptor labelDescriptor : _excludedLabels) {
      if (node.hasLabel(labelDescriptor.label())) {
        return false;
      }
    }
    return true;
  }

  private boolean matchesRelationships(Node node) {
    for (RelationshipDescriptor relDesc : _requiredRelationships) {
      if (
        !node.hasRelationship(relDesc.direction(), relDesc.relationshipType())
      ) {
        return false;
      }
    }
    for (RelationshipDescriptor relDesc : _excludedRelationships) {
      if (
        node.hasRelationship(relDesc.direction(), relDesc.relationshipType())
      ) {
        return false;
      }
    }
    return true;
  }

  private boolean matchesProperties(Node node) {
    for (PropertyDescriptor<?> propertyDescriptor : _requiredProperties) {
      if (!propertyDescriptor.appliesTo(node)) {
        return false;
      }
    }
    for (PropertyDescriptor<?> propertyDescriptor : _excludedProperties) {
      if (propertyDescriptor.appliesTo(node)) {
        return false;
      }
    }
    return true;
  }

  private boolean matchesPropertyValues(Node node) {
    for (PropertyValuePair<?> propertyValueDescriptor : _requiredPropValuePairs) {
      if (!propertyValueDescriptor.appliesTo(node)) {
        return false;
      }
    }
    return true;
  }

  public boolean isMatch(Node node) {
    if (node == null) {
      throw new IllegalArgumentException("node must not be null");
    }
    if (!matchesLabels(node)) {
      return false;
    }
    if (!matchesRelationships(node)) {
      return false;
    }
    if (!matchesProperties(node)) {
      return false;
    }
    if (!matchesPropertyValues(node)) {
      return false;
    }
    return true;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null || that.getClass() != getClass()) {
      return false;
    }
    NodeIdentifier thatSig = (NodeIdentifier) that;
    if (hashCode() != thatSig.hashCode()) {
      return false;
    }
    if (!_requiredLabels.equals(thatSig._requiredLabels)) {
      return false;
    }
    if (!_excludedLabels.equals(thatSig._excludedLabels)) {
      return false;
    }
    if (!_requiredRelationships.equals(thatSig._requiredRelationships)) {
      return false;
    }
    if (!_excludedRelationships.equals(thatSig._excludedRelationships)) {
      return false;
    }
    if (!_requiredProperties.equals(thatSig._requiredProperties)) {
      return false;
    }
    if (!_excludedProperties.equals(thatSig._excludedProperties)) {
      return false;
    }
    if (!_requiredPropValuePairs.equals(thatSig._requiredPropValuePairs)) {
      return false;
    }
    return true;
  }

  private int computeHash() {
    // This code seems excessive, but I have no better idea. I really do need to sort these sets
    // for the hashCode computation if I want extensional equality

    int hash = 1;

    List<Integer> requiredLabelHashes = _requiredLabels
      .stream()
      .map(x -> x.hashCode())
      .collect(Collectors.toList());
    Collections.sort(requiredLabelHashes);
    for (Integer i : requiredLabelHashes) {
      hash = (hash * 397) ^ i;
    }

    List<Integer> excludedLabelHashes = _excludedLabels
      .stream()
      .map(x -> x.hashCode())
      .collect(Collectors.toList());
    Collections.sort(excludedLabelHashes);
    for (Integer i : excludedLabelHashes) {
      hash = (hash * 397) ^ i;
    }

    List<Integer> requiredRelationHashes = _requiredRelationships
      .stream()
      .map(x -> x.hashCode())
      .collect(Collectors.toList());
    Collections.sort(requiredRelationHashes);
    for (Integer i : requiredRelationHashes) {
      hash = (hash * 397) ^ i;
    }

    List<Integer> excludedRelationHashes = _excludedRelationships
      .stream()
      .map(x -> x.hashCode())
      .collect(Collectors.toList());
    Collections.sort(excludedRelationHashes);
    for (Integer i : excludedRelationHashes) {
      hash = (hash * 397) ^ i;
    }

    List<Integer> requiredPropertyHashes = _requiredProperties
      .stream()
      .map(x -> x.hashCode())
      .collect(Collectors.toList());
    Collections.sort(requiredPropertyHashes);
    for (Integer i : requiredPropertyHashes) {
      hash = (hash * 397) ^ i;
    }

    List<Integer> excludedPropertyHashes = _excludedProperties
      .stream()
      .map(x -> x.hashCode())
      .collect(Collectors.toList());
    Collections.sort(excludedPropertyHashes);
    for (Integer i : excludedPropertyHashes) {
      hash = (hash * 397) ^ i;
    }

    List<Integer> requiredPropertyValuePairHashes = _requiredPropValuePairs
      .stream()
      .map(x -> x.hashCode())
      .collect(Collectors.toList());
    Collections.sort(requiredPropertyValuePairHashes);
    for (Integer i : requiredPropertyValuePairHashes) {
      hash = (hash * 397) ^ i;
    }

    hash = (hash * 397) ^ Boolean.hashCode(_isFinal);
    return hash;
  }

  @Override
  public int hashCode() {
    if (_hash != null) {
      return _hash;
    }
    if (!_isFinal) {
      return computeHash();
    } else {
      _hash = computeHash();
      return _hash;
    }
  }
}
