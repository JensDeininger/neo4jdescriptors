package movieDescriptors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.neo4j.driver.Config;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Session;
import org.neo4j.harness.Neo4j;
import org.neo4j.harness.Neo4jBuilders;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SampleProceduresTest {

  private static final Config driverConfig = Config
    .builder()
    .withoutEncryption()
    .build();
  private Driver driver;
  private Neo4j embeddedDatabaseServer;

  @BeforeAll
  void initializeNeo4j() throws IOException {
    var sw = new StringWriter();
    //     /MasterGraphProposal.cypher
    //     /TestSetAllPropertyNodes.cypher
    try (
      var in = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream("/movie.cypher"))
      )
    ) {
      in.transferTo(sw);
      sw.flush();
    }

    this.embeddedDatabaseServer =
      Neo4jBuilders
        .newInProcessBuilder()
        .withProcedure(movieDescriptors.sampleProcedures.SampleProcedures.class)
        .withProcedure(movieDescriptors.NodeValidation.class)
        .withFixture(sw.toString())
        .build();

    driver =
      GraphDatabase.driver(embeddedDatabaseServer.boltURI(), driverConfig);
    try (Session session = driver.session()) {
      //session.run( "MATCH (p:Project) UNWIND p AS node CALL org.rle.sdp3.setNumberClayOrVirtualModels(node) RETURN p" );
    }
    System.out.println("Database initialized.");
  }

  @AfterAll
  void closeDriver() {
    driver.close();
    embeddedDatabaseServer.close();

    System.out.println("Everything closed.");
  }

  @Test
  public void getPersonsYearBornTest() {
    try (Session session = driver.session()) {
      session.run("CALL getPersonsYearBorn(1965)");
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  @Test
  public void setActedInMovieCountTest() {
    try (Session session = driver.session()) {
      session.run("CALL setActedInMovieCount()");
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  @Test
  public void doLazyActionReviewsTest() {
    try (Session session = driver.session()) {
      // run( "MATCH (a:Activity) UNWIND a as n  CALL org.rle.sdp3.calculateActiveNodesCost(n) RETURN n" );
      session.run(
        "MATCH (p:Person {name: 'Jessica Thompson'}) UNWIND p as n CALL doLazyActionReviews(n) RETURN n"
      );
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
